﻿namespace RandomStudent
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BT_Random = new System.Windows.Forms.Button();
            this.TB_Amount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BT_Random
            // 
            this.BT_Random.Location = new System.Drawing.Point(236, 102);
            this.BT_Random.Name = "BT_Random";
            this.BT_Random.Size = new System.Drawing.Size(75, 23);
            this.BT_Random.TabIndex = 0;
            this.BT_Random.Text = " Random";
            this.BT_Random.UseVisualStyleBackColor = true;
            this.BT_Random.Click += new System.EventHandler(this.BT_Random_Click);
            // 
            // TB_Amount
            // 
            this.TB_Amount.Location = new System.Drawing.Point(73, 60);
            this.TB_Amount.Name = "TB_Amount";
            this.TB_Amount.Size = new System.Drawing.Size(238, 20);
            this.TB_Amount.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số lượng";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 169);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_Amount);
            this.Controls.Add(this.BT_Random);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BT_Random;
        private System.Windows.Forms.TextBox TB_Amount;
        private System.Windows.Forms.Label label1;
    }
}

