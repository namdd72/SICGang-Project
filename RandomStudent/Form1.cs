﻿using SICGang.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace RandomStudent
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BT_Random_Click(object sender, EventArgs e)
        {
            RandomStudents();
        }

        public void RandomStudents()
        {
            var ListStudentID = Directory.GetFiles(@"D:\THE SINH VIEN\CAC KHOA\anhthesinhvien");
            var ListFaculties = SQL_Sever.ExecuteDataTable("select * from tb_Faculties").AsEnumerable().Select(Row => Faculties.Parse(Row)).ToList();
            var ListClasses = new List<List<Classes>>();

            foreach (var Faculty in ListFaculties)
                ListClasses.Add(SQL_Sever.ExecuteDataTable($"select * from tb_Classes where FacultyID = {Faculty.GetFacultyID()}").AsEnumerable().Select(Row => Classes.Parse(Row)).ToList());

            var Rand = new Random();
            var Student = new Students();
            int MaxRand = int.Parse(TB_Amount.Text);

            for (int i = 0; i<MaxRand; i++)
            {
                try
                {
                    do
                    {
                        string filename = ListStudentID[Rand.Next(ListStudentID.Count())];
                        Student.SetStudentID(filename.Substring(filename.LastIndexOf(@"\")+1, 10));
                    }
                    while (Student.GetStudentID().Length != 10 || Student.GetStudentID().Contains(" "));

                    Student.SetName(MyHelper.RandomName());
                    Student.SetDateOfBirth(MyHelper.RandomDateOfBirth());
                    Student.SetClassID(ListClasses[Rand.Next(ListFaculties.Count())][Rand.Next(ListClasses.Count)].GetClassID());

                    Student.Post();
                }
                catch { }
            }

            MessageBox.Show("Done!");
        }

        private void RandomClasses()
        {
            var ListFaculties = SQL_Sever.ExecuteDataTable("select * from tb_Faculties").AsEnumerable().Select(Row => SICGang.Models.Faculties.Parse(Row)).ToList();

            var Class = new Classes();
            var Rand = new Random();
            int Count = 0, MaxRand = int.Parse(TB_Amount.Text);

            foreach (var Fuclulty in ListFaculties)
            {
                int AmountClasses = Rand.Next(MaxRand);
                for (int i = 0; i < AmountClasses; i++)
                {
                    Class.SetName($"Class {Count++}");
                    Class.SetFacultyID(Fuclulty.GetFacultyID());
                    Class.Post();
                }
            }

            MessageBox.Show("Done!");
        }
    }
}
