﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SICGang.Models
{
    class SQL_Sever
    {
        private static string StrConn = @"Data Source=DESKTOP-JD9V1N2;Initial Catalog=SICGANG_Project;Integrated Security=True;";
        private static SqlConnection Conn = new SqlConnection(StrConn);
        private static SqlCommand Cmd = new SqlCommand();

        /// <summary>
        /// Opens Database Connection.
        /// </summary>
        public static void Connect()
        {
            if (Conn.State == ConnectionState.Closed)
                Conn.Open();
        }

        /// <summary>
        /// Initializes a new instance of the SqlDataAdapter class with Query.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static SqlDataAdapter DataAdapter(string Query)
        {
            Connect();

            var Result = new SqlDataAdapter(Query, Conn);

            Disconnect();

            return Result;
        }

        /// <summary>
        /// Execute Query and returns result by DataTable.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static DataTable ExecuteDataTable(string Query)
        {
            var Result = new DataTable();

            DataAdapter(Query).Fill(Result);

            return Result;
        }

        /// <summary>
        /// Execute Query and returns a column in DataTable as List object.
        /// </summary>
        /// <param name="Query"></param>
        /// <param name="Column"></param>
        /// <returns></returns>
        public static List<object> ExecuteList(string Query, int Column = 0)
        {
            return ExecuteDataTable(Query).AsEnumerable().Select(Row => Row[Column]).ToList();
        }

        /// <summary>
        /// Execute Query and returns first row in result.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static DataRow ReadRow(string Query)
        {
            return ExecuteDataTable(Query).Rows[0];
        }

        /// <summary>
        /// Executes Query and returns first column of first row in result.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static object ExecuteScalar(string Query)
        {
            Connect();

            Cmd.Connection = Conn;
            Cmd.CommandText = Query;
            var Result = Cmd.ExecuteScalar();

            Disconnect();

            return Result;
        }

        /// <summary>
        /// Execute Query and returns number of rows affected.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static int ExecuteCommand(string Query)
        {
            Connect();

            Cmd.Connection = Conn;
            Cmd.CommandText = Query;
            int RowsAffected = Cmd.ExecuteNonQuery();

            Disconnect();

            return RowsAffected;
        }

        /// <summary>
        /// Closes Connection to Database.
        /// </summary>
        public static void Disconnect()
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
        }
    }
}