﻿using System;
using System.Text;
using System.Web.Security;

namespace SICGang.Models
{
    public static class MyHelper
    {
        public static string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, Convert.ToBase64String(Encoding.ASCII.GetBytes(salt)));
            return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha1");
        }

        public static string CreatePasswordHash(Accounts accounts) => CreatePasswordHash(accounts.GetPassword(), accounts.GetUser());

        private static Random Rand = new Random();

        private static string[] Ho = new string[]
            {
                "Dương","Lê","Bùi","Hoàng","Trịnh","Nguyễn","Đặng","Vũ",
                "Đỗ","Phạm","Chu","Trần","Cao","Đào","Hồ","Trương",
                "Phan","Thân","Ngô","Hà","Vi","Thái","Lý","Đinh",
                "Đoàn","Mai","Huỳnh"
            };

        private static string[] lotNam = new string[]
        {
                "Đức","Văn","Minh","Tuấn","Thế","Thái",
                "Xuân", "Đình","Cảnh","Quốc","Bá","Mạnh",
                "Quang","Tiến","Giang","Long","Nam","Thanh",
                "Thành","Vương","Duy","Anh","Hải","Nhất",
                "Khánh","Công","Trung","Tuấn","Việt","Hoàng",
                "Đình Mạnh","Kim","Khánh"
        };

        private static string[] lotNu = new string[]
        {
                "Thị","Thị Minh","Thái","Xuân","Thanh","Ngọc",
                "Hồng","Thị Hồng","Thị Ngọc","Thị Xuân","Thị Thanh","Thị Thu",
                "Thu","Như","Thị Như","Thị Ánh","Thị Lan","Thị Phương",
                "Thị Ngọc","Thị Tố","Tố","Mai","Hải","Thị Hải",
                "Bích","Thị Bích","Phương","Thùy","Thị Thùy",
                "Thị Mỹ","Mỹ","Thị Kim","Thị Cẩm","Cẩm","Lan",
                "Nhật","Thị Nhật"
        };

        private static string[] tenNam = new string[]
        {
                "Nam","Anh","Bảo","Bắc","Chiến","Diêm","Duy",
                "Du","Dương","Đạt","Hải","Hiệp","Hiếu",
                "Hinh","Hưng","Huynh","Kiên","Kỷ","Lịch",
                "Linh","Lượng","Mạnh","Minh","Nghĩa","Ngọc",
                "Nhất","Phong","Phúc","Phương","Quang","Quát",
                "Quý","Sang","Thắng","Thịnh","Thọ","Thuật",
                "Toàn","Tôn","Trung","Trường","Tú","Tuấn",
                "Tùng","Việt","Long","Khải","Chinh","Vĩnh",
                "Vinh","Hậu","Hà","Huy","Công","Thái",
                "Đức","Khánh","Văn","Sơn","Chính","Tuyến",
                "Thế","Dự","Điệp","Hùng","Kiểm","Lâm",
                "Nhật","Phú","Sáng","Thành","Khang","Thiện",
                "Tuyên","Tiến"
        };

        private static string[] tenNu = new string[]
        {
                "Anh","Bảo","Diệp","Linh","Lý","Ngọc","Loan",
                "Nhung","Phương","Thanh","Thùy","Dương","Minh",
                "Thương","Mỹ","Tú","Tuyết","Trang","Thoa",
                "Thảo","Yến","Vui","Mai","Lam","Chinh",
                "Giang","Hà","Uyên","Thủy","Liên","Ngân",
                "Dung","Hiền","Thư","Bích","Vân","Nga",
                "Hằng","Huyền","Thúy","Uyển","Hồng","Oanh",
                "Duyên","Quỳnh","Hoa","Ánh","Phượng","Huế",
                "Lan","Thúy","Hiền"
        };

        private static string[] TinhThanh = new string[]
        {
            "An Giang","Bà Rịa – Vũng Tàu","Bắc Giang","Bắc Kạn","Bạc Liêu","Bắc Ninh","Bến Tre","Bình Định",
            "Bình Dương","Bình Phước","Bình Thuận","Cà Mau","Cần Thơ","Cao Bằng","Đà Nẵng","Đắk Lắk","Đắk Nông",
            "Điện Biên","Đồng Nai","Đồng Tháp","Gia Lai","Hà Giang","Hà Nam","Hà Nội","Hà Tĩnh","Hải Dương",
            "Hải Phòng","Hậu Giang","Hòa Bình","Hưng Yên","Khánh Hòa","Kiên Giang","Kon Tum","Lai Châu","Lâm Đồng",
            "Lạng Sơn","Lào Cai","Long An","Nam Định","Nghệ An","Ninh Bình","Ninh Thuận","Phú Thọ","Phú Yên",
            "Quảng Bình","Quảng Nam","Quảng Ngãi","Quảng Ninh","Quảng Trị","Sóc Trăng","Sơn La","Tây Ninh",
            "Thái Bình","Thái Nguyên","Thanh Hóa","Thừa Thiên Huế","Tiền Giang","TP Hồ Chí Minh","Trà Vinh",
            "Tuyên Quang","Vĩnh Long","Vĩnh Phúc","Yên Bái"
        };

        public static string RandomName()
        {
            if (Rand.Next(1000) % 2 != 0)
                return Ho[Rand.Next(Ho.Length)] + " " + lotNam[Rand.Next(lotNam.Length)] + " " + tenNam[Rand.Next(tenNam.Length)];

            return Ho[Rand.Next(Ho.Length)] + " " + lotNu[Rand.Next(lotNu.Length)] + " " + tenNu[Rand.Next(tenNu.Length)];
        }

        public static DateTime RandomDateOfBirth()
        {
            int Year, Month, Day, tempDay;

            Year = Rand.Next(10) + 1991;
            Month = Rand.Next(12) + 1;
            if (Month == 4 || Month == 6 || Month == 9 || Month == 11)
                tempDay = 30;
            else if (Month == 2) if (Year % 4 == 0 && Year % 100 != 0 || Year % 400 == 0)
                    tempDay = 29;
                else tempDay = 28;
            else tempDay = 31;
            Day = Rand.Next(tempDay) + 1;

            return new DateTime(Year, Month, Day);
        }
    }
}