﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;

namespace SICGang.Models
{
    public class Students
    {
        private string StudentID;
        private string Name;
        private DateTime DateOfBirth;
        private int ClassID;

        public Students() { }

        public Students(string _StudentID, string _Name, DateTime _DateOfBirth, int _ClassID)
        {
            SetStudentID(_StudentID);
            SetName(_Name);
            SetDateOfBirth(_DateOfBirth);
            SetClassID(_ClassID);
        }

        public string GetStudentID()
        {
            return StudentID;
        }

        public void SetStudentID(string value)
        {
            StudentID = value;
        }

        public string GetName()
        {
            return Name;
        }

        public void SetName(string value)
        {
            Name = value;
        }

        public DateTime GetDateOfBirth()
        {
            return DateOfBirth;
        }

        public string GetDateOfBirthString()
        {
            return DateOfBirth.ToShortDateString();
        }

        public void SetDateOfBirth(DateTime value)
        {
            DateOfBirth = value;
        }

        public int GetClassID()
        {
            return ClassID;
        }

        public void SetClassID(int value)
        {
            ClassID = value;
        }

        public string GetClassName()
        {
            var Class = Classes.Get(GetClassID());

            return Class.GetName();
        }

        public string GetFacultyName()
        {
            return Classes.GetFacultyName(GetClassName());
        }

        public string GetBase64IMG()
        {
            var StudentIMG = StudentIMGs.Get(GetStudentID());

            return StudentIMG.GetBase64IMG();
        }

        public Image GetIMG()
        {
            try
            {
                var StudentIMG = StudentIMGs.Get(GetStudentID());

                return StudentIMG.GetIMG();
            }
            catch { return null; }
        }

        /// <summary>
        /// Get Students from Database by Student ID.
        /// </summary>
        /// <param name="_StudentID"></param>
        /// <returns></returns>
        public static Students Get(string _StudentID)
        {
            string Query = $"select * from tb_Students  where StudentID = '{_StudentID}'";

            return Students.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Insert Students to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_Students values('{GetStudentID()}', N'{GetName()}', '{GetDateOfBirth()}', {GetClassID()})";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Update Students by Student ID.
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string Query = $"update tb_Students set [Name] = N'{GetName()}', DateOfBirth = '{GetDateOfBirth()}', ClassID = {GetClassID()} where StudentID = '{GetStudentID()}'";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Convert DataRow to Students.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static Students Parse(DataRow row)
        {
            return new Students()
            {
                StudentID = Convert.ToString(row[0]),
                Name = Convert.ToString(row[1]),
                DateOfBirth = Convert.ToDateTime(row[2]),
                ClassID = Convert.ToInt32(row[3])
            };
        }

        public static List<object> Find(string StudentID)
        {
            string Query = $"select StudentID from tb_Students where StudentID like '{StudentID}%'";

            return SQL_Sever.ExecuteList(Query);
        }

        public override bool Equals(object obj)
        {
            return obj is Students students &&
                   GetStudentID() == students.GetStudentID() &&
                   GetName() == students.GetName() &&
                   GetDateOfBirth() == students.GetDateOfBirth() &&
                   GetClassID() == students.GetClassID();
        }
    }
}