﻿using DevExpress.DataAccess.Excel;
using SICGang.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddStudent
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BT_Open_Click(object sender, EventArgs e)
        {
            if (OFD_OpenFile.ShowDialog() == DialogResult.OK)
            {
                TE_Filename.Text = OFD_OpenFile.FileName;
                EDS.FileName = OFD_OpenFile.FileName;
                EDS.SourceOptions = new ExcelSourceOptions(new ExcelWorksheetSettings("Sheet1"));
                EDS.Fill();
                DGV.DataSource = EDS;
            }
        }

        private void BT_Import_Click(object sender, EventArgs e)
        {
            //var classes = DGV.Rows.OfType<DataGridViewRow>().GroupBy(r => r.Cells["Lớp"].Value).Select(i => i.Key.ToString()).ToList();
            //var Class = new Classes();

            //foreach (var i in classes)
            //{
            //    try
            //    {
            //        Class.SetName(i);
            //        Class.SetFacultyID(Faculties.Get(CBE_Faculties.Text).GetFacultyID());
            //        Class.Post();
            //    }
            //    catch { }
            //}

            var student = new Students();
            foreach (DataGridViewRow i in DGV.Rows)
            {
                try
                {
                    student.SetStudentID(i.Cells["Mã số sinh viên"].Value.ToString());
                    student.SetName(MyHelper.TitleFormat($"{i.Cells["Họ"].Value.ToString()} {i.Cells["Tên"].Value.ToString()}"));
                    var a = i.Cells["Năm sinh"].Value;
                    student.SetDateOfBirth(Convert.ToDateTime(a));
                    student.SetClassID(Classes.Get(i.Cells["Lớp"].Value.ToString()).GetClassID());

                    student.Post();
                }
                catch { student.Update(); }
            }

            MessageBox.Show("Done");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CBE_Faculties.Properties.Items.Clear();
            CBE_Faculties.Properties.Items.AddRange(Faculties.GetFacultyNames());
        }
    }
}
