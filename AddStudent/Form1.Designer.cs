﻿namespace AddStudent
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BT_Import = new System.Windows.Forms.Button();
            this.BT_Open = new System.Windows.Forms.Button();
            this.TE_Filename = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DGV = new System.Windows.Forms.DataGridView();
            this.OFD_OpenFile = new System.Windows.Forms.OpenFileDialog();
            this.EDS = new DevExpress.DataAccess.Excel.ExcelDataSource();
            this.CBE_Faculties = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Filename.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.DGV, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CBE_Faculties);
            this.panel1.Controls.Add(this.BT_Import);
            this.panel1.Controls.Add(this.BT_Open);
            this.panel1.Controls.Add(this.TE_Filename);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 53);
            this.panel1.TabIndex = 0;
            // 
            // BT_Import
            // 
            this.BT_Import.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BT_Import.Location = new System.Drawing.Point(710, 16);
            this.BT_Import.Name = "BT_Import";
            this.BT_Import.Size = new System.Drawing.Size(75, 23);
            this.BT_Import.TabIndex = 2;
            this.BT_Import.Text = "Import";
            this.BT_Import.UseVisualStyleBackColor = true;
            this.BT_Import.Click += new System.EventHandler(this.BT_Import_Click);
            // 
            // BT_Open
            // 
            this.BT_Open.Location = new System.Drawing.Point(314, 16);
            this.BT_Open.Name = "BT_Open";
            this.BT_Open.Size = new System.Drawing.Size(75, 23);
            this.BT_Open.TabIndex = 2;
            this.BT_Open.Text = "Open";
            this.BT_Open.UseVisualStyleBackColor = true;
            this.BT_Open.Click += new System.EventHandler(this.BT_Open_Click);
            // 
            // TE_Filename
            // 
            this.TE_Filename.Location = new System.Drawing.Point(67, 18);
            this.TE_Filename.Name = "TE_Filename";
            this.TE_Filename.Size = new System.Drawing.Size(236, 20);
            this.TE_Filename.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "File name";
            // 
            // dataGridView1
            // 
            this.DGV.AllowUserToAddRows = false;
            this.DGV.AllowUserToDeleteRows = false;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.Location = new System.Drawing.Point(3, 62);
            this.DGV.Name = "dataGridView1";
            this.DGV.ReadOnly = true;
            this.DGV.Size = new System.Drawing.Size(794, 385);
            this.DGV.TabIndex = 1;
            // 
            // OFD_OpenFile
            // 
            this.OFD_OpenFile.FileName = "openFileDialog1";
            // 
            // EDS
            // 
            this.EDS.Name = "EDS";
            // 
            // CBE_Faculties
            // 
            this.CBE_Faculties.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CBE_Faculties.Location = new System.Drawing.Point(511, 18);
            this.CBE_Faculties.Name = "CBE_Faculties";
            this.CBE_Faculties.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Faculties.Size = new System.Drawing.Size(193, 20);
            this.CBE_Faculties.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Filename.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BT_Import;
        private System.Windows.Forms.Button BT_Open;
        private DevExpress.XtraEditors.TextEdit TE_Filename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.OpenFileDialog OFD_OpenFile;
        private DevExpress.DataAccess.Excel.ExcelDataSource EDS;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Faculties;
    }
}

