﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SICGang.Models
{
    class Scan
    {
        private DateTime Time;
        private string Text = "";

        public Scan() { Time = DateTime.Now; }

        public Scan(string text)
        {
            Time = DateTime.Now;
            Text = text;
        }

        public string GetText()
        {
            return Text;
        }

        public void AddText(char tmp)
        {
            if (Time.AddSeconds(1) > DateTime.Now && Text.Length < 10) Text += tmp;
            else
            {
                Time = DateTime.Now;
                Text = tmp.ToString();
            }
        }
    }
}