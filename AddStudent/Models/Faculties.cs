﻿using System;
using System.Collections.Generic;
using System.Data;

namespace SICGang.Models
{
    public class Faculties
    {
        private int FacultyID;
        private string Name;

        public Faculties() { }

        public Faculties(int _FacultyID, string _Name)
        {
            SetFacultyID(_FacultyID);
            SetName(_Name);
        }

        public int GetFacultyID()
        {
            return FacultyID;
        }

        public void SetFacultyID(int value)
        {
            FacultyID = value;
        }

        public string GetName()
        {
            return Name;
        }

        public void SetName(string value)
        {
            Name = value;
        }

        /// <summary>
        /// Get Faculties from Database by Faculty ID.
        /// </summary>
        /// <param name="_FacultyID"></param>
        /// <returns></returns>
        public static Faculties Get(int _FacultyID)
        {
            string Query = $"select * from tb_Faculties where FacultyID = {_FacultyID}";

            return Faculties.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// get Faculties from Database by Faculty Name.
        /// </summary>
        /// <param name="_Facultyname"></param>
        /// <returns></returns>
        public static Faculties Get(string _Facultyname)
        {
            string Query = $"select * from tb_Faculties where [Name] = N'{_Facultyname}'";

            return Faculties.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Insert Faculties to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_Faculties([Name]) values(N'{GetName()}')";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Get all Faculties Name in Database.
        /// </summary>
        /// <returns></returns>
        public static List<object> GetFacultyNames()
        {
            string Query = "select [Name] from tb_Faculties";

            return SQL_Sever.ExecuteList(Query);
        }

        /// <summary>
        /// Get all Class Name by Faculty Name
        /// </summary>
        /// <param name="_FacultyName"></param>
        /// <returns></returns>
        public static List<object> GetClassNames(string _FacultyName)
        {
            string Query = $"select * from func_GetClassesFromFacultyName(N'{_FacultyName}')";

            return SQL_Sever.ExecuteList(Query);
        }

        /// <summary>
        /// Convert DataRow to Faculties.
        /// </summary>
        /// <param name="_Row"></param>
        /// <returns></returns>
        public static Faculties Parse(DataRow _Row)
        {
            return new Faculties()
            {
                FacultyID = Convert.ToInt32(_Row[0]),
                Name = Convert.ToString(_Row[1])
            };
        }

        public static bool Exists(string _FacultyName)
        {
            try
            {
                Faculties.Get(_FacultyName);
                return true;
            }
            catch { return false; }
        }
    }
}