﻿using System;
using System.Data;

namespace SICGang.Models
{
    public class Accounts
    {
        private string StudentID;
        private string User;
        private string Password;
        private bool isAdmin;

        public Accounts() { }

        public Accounts(string studentId, string user, string password, bool isAdmin)
        {
            SetStudentID(studentId);
            SetUser(user);
            SetPassword(password);
            SetIsAdmin(isAdmin);
        }

        public string GetStudentID()
        {
            return StudentID;
        }

        public void SetStudentID(string value)
        {
            StudentID = value;
        }

        public string GetUser()
        {
            return User;
        }

        public void SetUser(string value)
        {
            User = value;
        }

        public string GetPassword()
        {
            return Password;
        }

        public void SetPassword(string value)
        {
            Password = value;
        }

        public bool GetIsAdmin()
        {
            return isAdmin;
        }

        public void SetIsAdmin(bool value)
        {
            isAdmin = value;
        }

        public string GetNameUser()
        {
            return Students.Get(StudentID).GetName();
        }

        /// <summary>
        /// Check Username and Password in Database and return { -1: Not found, 0: Normal user, 1: Administrator }.
        /// </summary>
        /// <param name="_Username"></param>
        /// <param name="_Pass"></param>
        /// <returns></returns>
        public static int CheckAccount(string _Username, string _Pass)
        {
            string Query = $"select dbo.func_Login('{_Username}', '{_Pass}')";

            return (int)SQL_Sever.ExecuteScalar(Query);
        }

        /// <summary>
        /// Get Accounts from Database by Username and Password.
        /// </summary>
        /// <param name="_Username"></param>
        /// <param name="_Pass"></param>
        /// <returns></returns>
        public static Accounts Get(string _Username, string _Pass)
        {
            string Query = $"select * from dbo.func_GetAccount('{_Username}','{_Pass}')";

            return Accounts.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Insert Accounts to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_Accounts values('{GetStudentID()}', '{GetUser()}', '{GetPassword()}', {(GetIsAdmin()?1:0)})";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Update Accounts to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string Query = $"update tb_Accounts set isAdmin = {(GetIsAdmin()?1:0)} where [User] = '{GetUser()}'";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Delete Accounts to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            string Query = $"delete tb_Accounts where [User] = '{GetUser()}'";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Convert DataRow to Accounts.
        /// </summary>
        /// <param name="_Row"></param>
        /// <returns></returns>
        public static Accounts Parse(DataRow _Row)
        {
            return new Accounts()
            {
                StudentID = Convert.ToString(_Row[0]),
                User = Convert.ToString(_Row[1]),
                Password = Convert.ToString(_Row[2]),
                isAdmin = Convert.ToBoolean(_Row[3])
            };
        }

        public static bool Exist(string _Username)
        {
            try
            {
                string Query = $"select [User] from tb_Accounts where [User] = '{_Username}'";

                return (SQL_Sever.ExecuteScalar(Query) != null);
            }
            catch { return false; }
        }

        public override bool Equals(object obj)
        {
            return obj is Accounts accounts &&
                   GetStudentID() == accounts.GetStudentID() &&
                   GetUser() == accounts.GetUser() &&
                   GetPassword() == accounts.GetPassword() &&
                   GetIsAdmin() == accounts.GetIsAdmin();
        }
    }
}