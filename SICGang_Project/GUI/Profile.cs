﻿using SICGang.Models;
using System;
using System.Windows.Forms;

namespace SICGang.GUI
{
    public partial class Profile : Form
    {
        private Accounts UserAccount = null;
        public Profile(Accounts _UserAccount)
        {
            InitializeComponent();
            UserAccount = _UserAccount;
        }

        private void FillData_CBE_Faculties()
        {
            CBE_Faculties.Properties.Items.Clear();
            CBE_Faculties.Properties.Items.AddRange(Faculties.GetFacultyNames());
        }

        private void FillData_CBE_Classes()
        {
            CBE_Classes.Properties.Items.Clear();
            var ClassNames = Faculties.GetClassNames(CBE_Faculties.Text);
            CBE_Classes.Properties.Items.AddRange(ClassNames);
        }

        private void BT_ChangePass_Click(object sender, EventArgs e)
        {
            new ChangePassword(UserAccount).ShowDialog();
        }

        private void BT_Modify_Click(object sender, EventArgs e)
        {
            switch (BT_Modify.Text)
            {
                case "Modify":
                    TE_Name.Enabled = true;
                    DE_DateOfBirth.Enabled = true;
                    CBE_Faculties.Enabled = true;
                    CBE_Classes.Enabled = true;

                    DE_DateOfBirth.Properties.MinValue = new DateTime(DateTime.Now.Year - 28, 1, 1);
                    DE_DateOfBirth.Properties.MaxValue = new DateTime(DateTime.Now.Year - 18, 12, DateTime.DaysInMonth(DateTime.Now.Year - 18, 12));

                    FillData_CBE_Faculties();
                    FillData_CBE_Classes();

                    BT_Modify.Text = "Save";

                    break;
                case "Save":
                    if (GetStudentFromField() != null)
                    {
                        TE_Name.Enabled = false;
                        DE_DateOfBirth.Enabled = false;
                        CBE_Faculties.Enabled = false;
                        CBE_Classes.Enabled = false;
                        BT_Modify.Text = "Modify";

                        SaveStudent();
                        SaveIMG();
                    }
                    else MessageBox.Show("Thông tin bị thiếu!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    break;
            }
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            LL_Username.Text = UserAccount.GetUser();
            TE_StudentID.Text = UserAccount.GetStudentID();
        }

        #region CBE Faculties Classes
        private void CBE_Classes_TextChanged(object sender, EventArgs e)
        {
            BT_AddClass.Visible = (CBE_Classes.SelectedIndex == -1 && !string.IsNullOrEmpty(CBE_Classes.Text) && BT_Modify.Text == "Save");
        }

        private void CBE_Faculties_SelectedIndexChanged(object sender, EventArgs e)
        {
            BT_AddFaculty.Visible = (CBE_Faculties.SelectedIndex == -1 && !string.IsNullOrEmpty(CBE_Faculties.Text));

            FillData_CBE_Classes();
        }

        private void CBE_Faculties_Leave(object sender, EventArgs e)
        {
            var Student = Students.Get(TE_StudentID.Text);
            CBE_Classes.Text = Student.GetFacultyName() != CBE_Faculties.Text ? "" : CBE_Classes.Text = Student.GetClassName();

            if (CBE_Faculties.SelectedIndex != -1)
            {
                CBE_Classes.Properties.Items.Clear();
                BT_AddFaculty.Hide();

                var ClassNames = Faculties.GetClassNames(CBE_Faculties.Text);

                if (ClassNames.Count != 0)
                {
                    BT_AddClass.Hide();
                    CBE_Classes.Properties.Items.AddRange(ClassNames);
                }
                else BT_AddClass.Show();
            }
            else
            {
                CBE_Faculties.Text = MyHelper.TitleFormat(CBE_Faculties.Text);

                if (string.IsNullOrEmpty(CBE_Faculties.Text))
                {
                    BT_AddFaculty.Hide();
                    BT_AddClass.Hide();
                }
                else
                {
                    BT_AddFaculty.Show();
                    BT_AddClass.Show();
                }
            }
        }

        #region BT Add
        private void BT_AddFaculty_Click(object sender, EventArgs e)
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var Faculty = new Faculties();
            Faculty.SetName(CBE_Faculties.Text);

            if (MessageBox.Show($"Xác nhận thêm khoa {Faculty.GetName()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (Faculty.Post())
                {
                    BT_AddFaculty.Hide();
                    FillData_CBE_Faculties();
                    Datalog.SetModifiedContent($"Thêm khoa {Faculty.GetName()}");
                    Datalog.Post();
                    MessageBox.Show($"Đã thêm khoa {Faculty.GetName()} thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show($"Thêm khoa {Faculty.GetName()} lỗi!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            CBE_Faculties.Select();
        }

        private void BT_AddClass_Click(object sender, EventArgs e)
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var Class = new Classes();
            Class.SetName(CBE_Classes.Text);
            Class.SetFacultyID(Faculties.Get(CBE_Faculties.Text).GetFacultyID());

            if (MessageBox.Show($"Xác nhận thêm lớp {Class.GetName()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (Class.Post())
                {
                    BT_AddClass.Hide();
                    FillData_CBE_Classes();
                    Datalog.SetModifiedContent($"Thêm lớp {Class.GetName()}");
                    Datalog.Post();
                    MessageBox.Show($"Đã thêm lớp {Class.GetName()} thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show($"Thêm lớp {Class.GetName()} lỗi!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            CBE_Classes.Select();
        }
        #endregion

        #endregion

        private void SaveStudent()
        {
            var StudentInput = GetStudentFromField();

            if (!StudentInput.Update())
                MessageBox.Show("Cập nhận thông tin lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SaveIMG()
        {
            if (IMGisOpen)
            {
                var StudentIMG = new StudentIMGs();
                StudentIMG.SetStudentID(TE_StudentID.Text);

                StudentIMG.SetIMG(PE_IMGStudent.Image);

                try { StudentIMG.Post(); }
                catch { StudentIMG.Update(); }

                IMGisOpen = false;
            }
        }

        private void LL_Username_Click(object sender, EventArgs e)
        {
            new ChangeUser(UserAccount).ShowDialog();
            LL_Username.Text = UserAccount.GetUser();
        }

        private void LL_Username_SizeChanged(object sender, EventArgs e)
        {
            PB_RepairIcon.Location = new System.Drawing.Point()
            {
                X = LL_Username.Location.X + LL_Username.Size.Width,
                Y = LL_Username.Location.Y
            };
        }
    }
}