﻿using System;
using System.Windows.Forms;
using SICGang.Models;

namespace SICGang.GUI
{
    public partial class Login : Form
    {
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TB_User.Text) || string.IsNullOrEmpty(TB_Pass.Text))
            {
                if (string.IsNullOrEmpty(TB_Pass.Text))
                {
                    ErrProvider.SetError(TB_Pass, "Chưa nhập mật khẩu ! ! !");
                    TB_Pass.Select();
                }

                if (string.IsNullOrEmpty(TB_User.Text))
                {
                    ErrProvider.SetError(TB_User, "Chưa nhập tên tài khoản ! ! !");
                    TB_User.Select();
                }
            }
            else
            {
                string User = TB_User.Text;
                string Password = MyHelper.CreatePasswordHash(TB_Pass.Text, TB_User.Text);

                switch (Accounts.CheckAccount(User, Password))
                {
                    case 0:
                    case 1:
                        this.Hide();

                        var UserAccount = Accounts.Get(User, Password);

                        new Main(UserAccount).ShowDialog();

                        ResetForm();

                        this.Show();
                        break;
                    default:
                        LB_LoginFalse.Show();
                        break;
                }
            }
        }
    }
}