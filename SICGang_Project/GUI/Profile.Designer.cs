﻿using SICGang.Models;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Profile));
            this.PE_IMGStudent = new DevExpress.XtraEditors.PictureEdit();
            this.BT_ChangePass = new CustomControls.MyButton();
            this.BT_Modify = new CustomControls.MyButton();
            this.BT_Exit = new CustomControls.BT_Exit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PB_RepairIcon = new System.Windows.Forms.PictureBox();
            this.LL_Username = new System.Windows.Forms.LinkLabel();
            this.XOFD = new DevExpress.XtraEditors.XtraOpenFileDialog(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.BT_AddClass = new System.Windows.Forms.Button();
            this.BT_AddFaculty = new System.Windows.Forms.Button();
            this.CBE_Classes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CBE_Faculties = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TE_Name = new DevExpress.XtraEditors.TextEdit();
            this.TE_StudentID = new DevExpress.XtraEditors.TextEdit();
            this.DE_DateOfBirth = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.PE_IMGStudent.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_RepairIcon)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Classes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_StudentID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // PE_IMGStudent
            // 
            this.PE_IMGStudent.EditValue = ((object)(resources.GetObject("PE_IMGStudent.EditValue")));
            this.PE_IMGStudent.Location = new System.Drawing.Point(49, 41);
            this.PE_IMGStudent.Name = "PE_IMGStudent";
            this.PE_IMGStudent.Properties.AllowFocused = false;
            this.PE_IMGStudent.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.PE_IMGStudent.Properties.Appearance.Options.UseBackColor = true;
            this.PE_IMGStudent.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.PE_IMGStudent.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Transparent;
            this.PE_IMGStudent.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.PE_IMGStudent.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.PE_IMGStudent.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PE_IMGStudent.Properties.OptionsMask.MaskLayoutMode = DevExpress.XtraEditors.Controls.PictureEditMaskLayoutMode.MiddleCenter;
            this.PE_IMGStudent.Properties.OptionsMask.MaskType = DevExpress.XtraEditors.Controls.PictureEditMaskType.Circle;
            this.PE_IMGStudent.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.PE_IMGStudent.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.PE_IMGStudent.Size = new System.Drawing.Size(130, 150);
            this.PE_IMGStudent.TabIndex = 2;
            this.PE_IMGStudent.Click += new System.EventHandler(this.PE_IMGStudent_Click);
            // 
            // BT_ChangePass
            // 
            this.BT_ChangePass.Location = new System.Drawing.Point(412, 368);
            this.BT_ChangePass.Name = "BT_ChangePass";
            this.BT_ChangePass.Size = new System.Drawing.Size(120, 40);
            this.BT_ChangePass.TabIndex = 5;
            this.BT_ChangePass.TabStop = false;
            this.BT_ChangePass.Text = " Change Password";
            this.BT_ChangePass.UseVisualStyleBackColor = true;
            this.BT_ChangePass.Click += new System.EventHandler(this.BT_ChangePass_Click);
            // 
            // BT_Modify
            // 
            this.BT_Modify.Location = new System.Drawing.Point(187, 368);
            this.BT_Modify.Name = "BT_Modify";
            this.BT_Modify.Size = new System.Drawing.Size(120, 40);
            this.BT_Modify.TabIndex = 6;
            this.BT_Modify.TabStop = false;
            this.BT_Modify.Text = "Modify";
            this.BT_Modify.UseVisualStyleBackColor = true;
            this.BT_Modify.Click += new System.EventHandler(this.BT_Modify_Click);
            // 
            // BT_Exit
            // 
            this.BT_Exit.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Exit.Location = new System.Drawing.Point(600, 0);
            this.BT_Exit.Name = "BT_Exit";
            this.BT_Exit.Size = new System.Drawing.Size(18, 18);
            this.BT_Exit.TabIndex = 17;
            this.BT_Exit.TabStop = false;
            this.BT_Exit.Text = "X";
            this.BT_Exit.UseVisualStyleBackColor = false;
            this.BT_Exit.Click += new System.EventHandler(this.BT_Exit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.PB_RepairIcon);
            this.panel1.Controls.Add(this.LL_Username);
            this.panel1.Controls.Add(this.BT_Exit);
            this.panel1.Location = new System.Drawing.Point(10, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(618, 138);
            this.panel1.TabIndex = 18;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseUp);
            // 
            // PB_RepairIcon
            // 
            this.PB_RepairIcon.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PB_RepairIcon.BackgroundImage")));
            this.PB_RepairIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PB_RepairIcon.Location = new System.Drawing.Point(293, 105);
            this.PB_RepairIcon.Name = "PB_RepairIcon";
            this.PB_RepairIcon.Size = new System.Drawing.Size(20, 20);
            this.PB_RepairIcon.TabIndex = 20;
            this.PB_RepairIcon.TabStop = false;
            this.PB_RepairIcon.Visible = false;
            // 
            // LL_Username
            // 
            this.LL_Username.AutoSize = true;
            this.LL_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LL_Username.LinkColor = System.Drawing.Color.White;
            this.LL_Username.Location = new System.Drawing.Point(187, 103);
            this.LL_Username.Name = "LL_Username";
            this.LL_Username.Size = new System.Drawing.Size(110, 25);
            this.LL_Username.TabIndex = 19;
            this.LL_Username.TabStop = true;
            this.LL_Username.Text = "Username";
            this.LL_Username.SizeChanged += new System.EventHandler(this.LL_Username_SizeChanged);
            this.LL_Username.Click += new System.EventHandler(this.LL_Username_Click);
            this.LL_Username.MouseEnter += new System.EventHandler(this.LL_Username_MouseEnter);
            this.LL_Username.MouseLeave += new System.EventHandler(this.LL_Username_MouseLeave);
            // 
            // XOFD
            // 
            this.XOFD.Filter = "JPeg Image|*.jpg|PNG Image|*.png|Bitmap Image|*.bmp";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::SICGang.Properties.Resources.FeildStudent;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.BT_AddClass);
            this.panel2.Controls.Add(this.BT_AddFaculty);
            this.panel2.Controls.Add(this.CBE_Classes);
            this.panel2.Controls.Add(this.CBE_Faculties);
            this.panel2.Controls.Add(this.labelControl4);
            this.panel2.Controls.Add(this.labelControl5);
            this.panel2.Controls.Add(this.labelControl3);
            this.panel2.Controls.Add(this.labelControl2);
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Controls.Add(this.TE_Name);
            this.panel2.Controls.Add(this.TE_StudentID);
            this.panel2.Controls.Add(this.DE_DateOfBirth);
            this.panel2.Location = new System.Drawing.Point(176, 179);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(413, 150);
            this.panel2.TabIndex = 19;
            // 
            // BT_AddClass
            // 
            this.BT_AddClass.BackgroundImage = global::SICGang.Properties.Resources.BT_Add;
            this.BT_AddClass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_AddClass.FlatAppearance.BorderSize = 0;
            this.BT_AddClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_AddClass.Location = new System.Drawing.Point(362, 121);
            this.BT_AddClass.Name = "BT_AddClass";
            this.BT_AddClass.Size = new System.Drawing.Size(20, 20);
            this.BT_AddClass.TabIndex = 5;
            this.BT_AddClass.TabStop = false;
            this.BT_AddClass.UseVisualStyleBackColor = true;
            this.BT_AddClass.Visible = false;
            this.BT_AddClass.Click += new System.EventHandler(this.BT_AddClass_Click);
            // 
            // BT_AddFaculty
            // 
            this.BT_AddFaculty.BackgroundImage = global::SICGang.Properties.Resources.BT_Add;
            this.BT_AddFaculty.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_AddFaculty.FlatAppearance.BorderSize = 0;
            this.BT_AddFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_AddFaculty.Location = new System.Drawing.Point(362, 74);
            this.BT_AddFaculty.Name = "BT_AddFaculty";
            this.BT_AddFaculty.Size = new System.Drawing.Size(20, 20);
            this.BT_AddFaculty.TabIndex = 5;
            this.BT_AddFaculty.TabStop = false;
            this.BT_AddFaculty.UseVisualStyleBackColor = true;
            this.BT_AddFaculty.Visible = false;
            this.BT_AddFaculty.Click += new System.EventHandler(this.BT_AddFaculty_Click);
            // 
            // CBE_Classes
            // 
            this.CBE_Classes.Enabled = false;
            this.CBE_Classes.Location = new System.Drawing.Point(194, 123);
            this.CBE_Classes.Name = "CBE_Classes";
            this.CBE_Classes.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.CBE_Classes.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CBE_Classes.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Classes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Classes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Classes.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CBE_Classes.Properties.Sorted = true;
            this.CBE_Classes.Size = new System.Drawing.Size(162, 18);
            this.CBE_Classes.TabIndex = 4;
            this.CBE_Classes.TextChanged += new System.EventHandler(this.CBE_Classes_TextChanged);
            this.CBE_Classes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputSpace);
            // 
            // CBE_Faculties
            // 
            this.CBE_Faculties.Enabled = false;
            this.CBE_Faculties.Location = new System.Drawing.Point(194, 75);
            this.CBE_Faculties.Name = "CBE_Faculties";
            this.CBE_Faculties.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.CBE_Faculties.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CBE_Faculties.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Faculties.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Faculties.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Faculties.Properties.Sorted = true;
            this.CBE_Faculties.Size = new System.Drawing.Size(162, 18);
            this.CBE_Faculties.TabIndex = 3;
            this.CBE_Faculties.SelectedIndexChanged += new System.EventHandler(this.CBE_Faculties_SelectedIndexChanged);
            this.CBE_Faculties.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputDigit);
            this.CBE_Faculties.Leave += new System.EventHandler(this.CBE_Faculties_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(194, 104);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(17, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Lớp";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(194, 57);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Khoa";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(47, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Ngày sinh";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Họ tên";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Mã SV";
            // 
            // TE_Name
            // 
            this.TE_Name.Enabled = false;
            this.TE_Name.Location = new System.Drawing.Point(11, 75);
            this.TE_Name.Name = "TE_Name";
            this.TE_Name.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.TE_Name.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.TE_Name.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_Name.Size = new System.Drawing.Size(162, 18);
            this.TE_Name.TabIndex = 1;
            this.TE_Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputDigit);
            this.TE_Name.Leave += new System.EventHandler(this.TE_Name_Leave);
            // 
            // TE_StudentID
            // 
            this.TE_StudentID.Enabled = false;
            this.TE_StudentID.Location = new System.Drawing.Point(11, 28);
            this.TE_StudentID.Name = "TE_StudentID";
            this.TE_StudentID.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.TE_StudentID.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.TE_StudentID.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_StudentID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_StudentID.Properties.MaxLength = 10;
            this.TE_StudentID.Size = new System.Drawing.Size(162, 18);
            this.TE_StudentID.TabIndex = 0;
            this.TE_StudentID.TextChanged += new System.EventHandler(this.TE_StudentID_TextChanged);
            // 
            // DE_DateOfBirth
            // 
            this.DE_DateOfBirth.EditValue = null;
            this.DE_DateOfBirth.Enabled = false;
            this.DE_DateOfBirth.Location = new System.Drawing.Point(11, 123);
            this.DE_DateOfBirth.Name = "DE_DateOfBirth";
            this.DE_DateOfBirth.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.DE_DateOfBirth.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.DE_DateOfBirth.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_DateOfBirth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_DateOfBirth.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_DateOfBirth.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_DateOfBirth.Properties.ShowToday = false;
            this.DE_DateOfBirth.Size = new System.Drawing.Size(162, 18);
            this.DE_DateOfBirth.TabIndex = 2;
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(640, 455);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.PE_IMGStudent);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BT_Modify);
            this.Controls.Add(this.BT_ChangePass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Profile";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Profile";
            this.TransparencyKey = System.Drawing.Color.WhiteSmoke;
            this.Load += new System.EventHandler(this.Profile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PE_IMGStudent.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_RepairIcon)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Classes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_StudentID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PictureEdit PE_IMGStudent;
        private CustomControls.MyButton BT_ChangePass;
        private CustomControls.MyButton BT_Modify;
        private CustomControls.BT_Exit BT_Exit;

        #region Custom Form
        #region TitleBar
        private void BT_Exit_Click(object sender, EventArgs e) => this.Close();

        #region Move
        bool isMouseDown;
        int xLast;
        int yLast;

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            xLast = e.X;
            yLast = e.Y;

            base.OnMouseDown(e);
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                int newY = this.Top + (e.Y - yLast);
                int newX = this.Left + (e.X - xLast);

                this.Location = new Point(newX, newY);
            }

            base.OnMouseMove(e);
        }

        private void TitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            base.OnMouseUp(e);
        }
        #endregion
        #endregion

        #region Textbox
        private void TE_Name_Leave(object sender, EventArgs e)
        {
            TE_Name.Text = MyHelper.TitleFormat(TE_Name.Text);
        }

        private void NotAllowInputDigit(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void NotAllowInputSpace(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
                e.Handled = true;
        }
        #endregion

        private void TE_StudentID_TextChanged(object sender, EventArgs e)
        {
            string StudentID = TE_StudentID.Text;

            var Student = Students.Get(StudentID);

            ExportDataToField(Student);

            PE_IMGStudent.Image = Student.GetIMG();
        }

        private void BT_AddFacultyClass_MouseEnter(object sender, EventArgs e)
        {
            (sender as Button).BackgroundImage = Properties.Resources.BT_Add_MouseEnter;
        }

        private void BT_AddFacultyClass_MouseLeave(object sender, EventArgs e)
        {
            (sender as Button).BackgroundImage = Properties.Resources.BT_Add;
        }

        private bool IMGisOpen = false;
        private void PE_IMGStudent_Click(object sender, EventArgs e)
        {
            if (BT_Modify.Text == "Save")
                if (XOFD.ShowDialog() == DialogResult.OK)
                {
                    PE_IMGStudent.Image = Image.FromFile(XOFD.FileName);
                    IMGisOpen = true;
                }
        }

        private Students GetStudentFromField()
        {
            if (string.IsNullOrEmpty(TE_StudentID.Text) || string.IsNullOrEmpty(TE_Name.Text) || string.IsNullOrEmpty(DE_DateOfBirth.Text) || string.IsNullOrEmpty(CBE_Classes.Text)) return null;
            else
            {
                var Student = new Students();

                Student.SetStudentID(TE_StudentID.Text);
                Student.SetName(TE_Name.Text);
                Student.SetDateOfBirth(Convert.ToDateTime(DE_DateOfBirth.EditValue));
                Student.SetClassID(Classes.Get(CBE_Classes.Text).GetClassID());

                return Student;
            }
        }

        private void ExportDataToField(Students _Student)
        {
            try
            {
                ExportDataToField(_Student.GetStudentID(), _Student.GetName(), _Student.GetDateOfBirthString(), _Student.GetFacultyName(), _Student.GetClassName());
            }
            catch { ExportDataToField(_Student.GetStudentID(), "", "", "", ""); }
        }

        private void ExportDataToField(string _StudentID, string _StudentName, string _DateOfBirth, string _FacultyName, string _ClassName)
        {
            TE_StudentID.Text = _StudentID;
            TE_Name.Text = _StudentName;
            DE_DateOfBirth.Text = _DateOfBirth.Split(' ')[0];
            CBE_Faculties.Text = _FacultyName;
            CBE_Classes.Text = _ClassName;
        }

        private void LL_Username_MouseEnter(object sender, EventArgs e) => PB_RepairIcon.Visible = true;

        private void LL_Username_MouseLeave(object sender, EventArgs e) => PB_RepairIcon.Visible = false;
        #endregion

        private Panel panel1;
        private DevExpress.XtraEditors.XtraOpenFileDialog XOFD;
        private LinkLabel LL_Username;
        private Panel panel2;
        private Button BT_AddClass;
        private Button BT_AddFaculty;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Classes;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Faculties;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit TE_Name;
        private DevExpress.XtraEditors.TextEdit TE_StudentID;
        private DevExpress.XtraEditors.DateEdit DE_DateOfBirth;
        private PictureBox PB_RepairIcon;
    }
}