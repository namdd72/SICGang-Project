﻿using SICGang.Models;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.TB_NewPass = new System.Windows.Forms.TextBox();
            this.BT_Change = new CustomControls.MyButton();
            this.BT_Exit = new CustomControls.BT_Exit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TB_ConfirmPass = new System.Windows.Forms.TextBox();
            this.TB_CurrentPass = new System.Windows.Forms.TextBox();
            this.ErrProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.LB_ErrConfirmPass = new System.Windows.Forms.Label();
            this.LB_CheckCurrentPassFail = new System.Windows.Forms.Label();
            this.Notify = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_NewPass
            // 
            this.TB_NewPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TB_NewPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_NewPass.Location = new System.Drawing.Point(166, 221);
            this.TB_NewPass.Name = "TB_NewPass";
            this.TB_NewPass.Size = new System.Drawing.Size(279, 19);
            this.TB_NewPass.TabIndex = 1;
            this.TB_NewPass.UseSystemPasswordChar = true;
            this.TB_NewPass.TextChanged += new System.EventHandler(this.TB_PassNew_TextChanged);
            // 
            // BT_Change
            // 
            this.BT_Change.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Change.Location = new System.Drawing.Point(166, 329);
            this.BT_Change.Name = "BT_Change";
            this.BT_Change.Size = new System.Drawing.Size(120, 40);
            this.BT_Change.TabIndex = 3;
            this.BT_Change.TabStop = false;
            this.BT_Change.Text = "Change";
            this.BT_Change.UseVisualStyleBackColor = true;
            this.BT_Change.Click += new System.EventHandler(this.BT_Change_Click);
            // 
            // BT_Exit
            // 
            this.BT_Exit.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Exit.Location = new System.Drawing.Point(458, 0);
            this.BT_Exit.Name = "BT_Exit";
            this.BT_Exit.Size = new System.Drawing.Size(18, 18);
            this.BT_Exit.TabIndex = 16;
            this.BT_Exit.TabStop = false;
            this.BT_Exit.Text = "X";
            this.BT_Exit.UseVisualStyleBackColor = false;
            this.BT_Exit.Click += new System.EventHandler(this.BT_Exit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.BT_Exit);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 123);
            this.panel1.TabIndex = 17;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseUp);
            // 
            // TB_ConfirmPass
            // 
            this.TB_ConfirmPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TB_ConfirmPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_ConfirmPass.Location = new System.Drawing.Point(166, 275);
            this.TB_ConfirmPass.Name = "TB_ConfirmPass";
            this.TB_ConfirmPass.Size = new System.Drawing.Size(279, 19);
            this.TB_ConfirmPass.TabIndex = 2;
            this.TB_ConfirmPass.UseSystemPasswordChar = true;
            this.TB_ConfirmPass.TextChanged += new System.EventHandler(this.TB_PassNew_TextChanged);
            // 
            // TB_CurrentPass
            // 
            this.TB_CurrentPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TB_CurrentPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_CurrentPass.Location = new System.Drawing.Point(166, 172);
            this.TB_CurrentPass.Name = "TB_CurrentPass";
            this.TB_CurrentPass.Size = new System.Drawing.Size(279, 19);
            this.TB_CurrentPass.TabIndex = 1;
            this.TB_CurrentPass.UseSystemPasswordChar = true;
            this.TB_CurrentPass.TextChanged += new System.EventHandler(this.TB_CurrentPass_TextChanged);
            // 
            // ErrProvider
            // 
            this.ErrProvider.ContainerControl = this;
            // 
            // LB_ErrConfirmPass
            // 
            this.LB_ErrConfirmPass.AutoSize = true;
            this.LB_ErrConfirmPass.BackColor = System.Drawing.Color.White;
            this.LB_ErrConfirmPass.Enabled = false;
            this.LB_ErrConfirmPass.ForeColor = System.Drawing.Color.Red;
            this.LB_ErrConfirmPass.Location = new System.Drawing.Point(222, 252);
            this.LB_ErrConfirmPass.Name = "LB_ErrConfirmPass";
            this.LB_ErrConfirmPass.Size = new System.Drawing.Size(162, 13);
            this.LB_ErrConfirmPass.TabIndex = 18;
            this.LB_ErrConfirmPass.Text = "Mật khẩu xác nhận không khớp!\r\n";
            this.LB_ErrConfirmPass.Visible = false;
            // 
            // LB_CheckCurrentPassFail
            // 
            this.LB_CheckCurrentPassFail.AutoSize = true;
            this.LB_CheckCurrentPassFail.BackColor = System.Drawing.Color.White;
            this.LB_CheckCurrentPassFail.Enabled = false;
            this.LB_CheckCurrentPassFail.ForeColor = System.Drawing.Color.Red;
            this.LB_CheckCurrentPassFail.Location = new System.Drawing.Point(233, 203);
            this.LB_CheckCurrentPassFail.Name = "LB_CheckCurrentPassFail";
            this.LB_CheckCurrentPassFail.Size = new System.Drawing.Size(139, 13);
            this.LB_CheckCurrentPassFail.TabIndex = 18;
            this.LB_CheckCurrentPassFail.Text = "Mật khẩu không chính xác!";
            this.LB_CheckCurrentPassFail.Visible = false;
            // 
            // Notify
            // 
            this.Notify.Icon = ((System.Drawing.Icon)(resources.GetObject("Notify.Icon")));
            this.Notify.Text = "SIC GANG";
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(500, 420);
            this.Controls.Add(this.LB_CheckCurrentPassFail);
            this.Controls.Add(this.LB_ErrConfirmPass);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BT_Change);
            this.Controls.Add(this.TB_ConfirmPass);
            this.Controls.Add(this.TB_CurrentPass);
            this.Controls.Add(this.TB_NewPass);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChangePassword";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Password";
            this.TransparencyKey = System.Drawing.Color.WhiteSmoke;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangePassword_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TB_NewPass;
        private CustomControls.MyButton BT_Change;
        private CustomControls.BT_Exit BT_Exit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox TB_ConfirmPass;
        private System.Windows.Forms.TextBox TB_CurrentPass;
        private System.Windows.Forms.ErrorProvider ErrProvider;
        private System.Windows.Forms.Label LB_ErrConfirmPass;
        private System.Windows.Forms.Label LB_CheckCurrentPassFail;

        #region Custom form
        public ChangePassword(Accounts _UserAccount)
        {
            InitializeComponent();
            UserAccount = _UserAccount;
        }

        #region TitleBar
        private void BT_Exit_Click(object sender, EventArgs e) => this.Close();

        #region Move
        bool isMouseDown;
        int xLast;
        int yLast;

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            xLast = e.X;
            yLast = e.Y;

            base.OnMouseDown(e);
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                int newY = this.Top + (e.Y - yLast);
                int newX = this.Left + (e.X - xLast);

                this.Location = new Point(newX, newY);
            }

            base.OnMouseMove(e);
        }

        private void TitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            base.OnMouseUp(e);
        }
        #endregion
        #endregion

        private void TB_PassNew_TextChanged(object sender, EventArgs e)
        {
            LB_ErrConfirmPass.Hide();
            ErrProvider.SetError((sender as Control), null);
        }

        private void TB_CurrentPass_TextChanged(object sender, EventArgs e)
        {
            LB_CheckCurrentPassFail.Hide();
            ErrProvider.SetError(TB_CurrentPass, null);
        }

        private void ChangePassword_FormClosing(object sender, FormClosingEventArgs e)
        {
            Notify.Visible = false;
        }
        #endregion

        private NotifyIcon Notify;
    }
}