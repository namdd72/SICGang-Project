﻿using System;
using System.Windows.Forms;

namespace SICGang.GUI.UserControls
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        ///
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.labelControl = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GC_View = new DevExpress.XtraGrid.GridControl();
            this.GV_View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.DE_EndDate = new DevExpress.XtraEditors.DateEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.DE_StartDate = new DevExpress.XtraEditors.DateEdit();
            this.BT_Export = new CustomControls.MyButton();
            this.BT_ViewAccessHistory = new CustomControls.MyButton();
            this.BT_ViewAdminDatalogs = new CustomControls.MyButton();
            this.XSFD = new DevExpress.XtraEditors.XtraSaveFileDialog(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GC_View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_View)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DE_EndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_EndDate.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DE_StartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_StartDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(418, 416);
            // 
            // labelControl
            // 
            this.labelControl.AllowHtmlString = true;
            this.labelControl.Appearance.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl.Appearance.Options.UseFont = true;
            this.labelControl.Appearance.Options.UseForeColor = true;
            this.labelControl.Appearance.Options.UseTextOptions = true;
            this.labelControl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl.LineLocation = DevExpress.XtraEditors.LineLocation.Left;
            this.labelControl.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl.LineVisible = true;
            this.labelControl.Location = new System.Drawing.Point(0, 0);
            this.labelControl.Name = "labelControl";
            this.labelControl.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.labelControl.Size = new System.Drawing.Size(800, 30);
            this.labelControl.TabIndex = 1;
            this.labelControl.Text = "View";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.GC_View, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 570);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // GC_View
            // 
            this.GC_View.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC_View.Location = new System.Drawing.Point(3, 140);
            this.GC_View.MainView = this.GV_View;
            this.GC_View.Name = "GC_View";
            this.GC_View.Size = new System.Drawing.Size(794, 427);
            this.GC_View.TabIndex = 0;
            this.GC_View.TabStop = false;
            this.GC_View.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV_View});
            // 
            // GV_View
            // 
            this.GV_View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GV_View.GridControl = this.GC_View;
            this.GV_View.Name = "GV_View";
            this.GV_View.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV_View.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV_View.OptionsBehavior.Editable = false;
            this.GV_View.OptionsFind.AlwaysVisible = true;
            this.GV_View.OptionsFind.FindDelay = 100;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.BT_Export);
            this.panel1.Controls.Add(this.BT_ViewAccessHistory);
            this.panel1.Controls.Add(this.BT_ViewAdminDatalogs);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 131);
            this.panel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::SICGang.Properties.Resources.TB;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.labelControl2);
            this.panel3.Controls.Add(this.DE_EndDate);
            this.panel3.Location = new System.Drawing.Point(349, 22);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(154, 47);
            this.panel3.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(67, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Ngày kết thúc";
            // 
            // DE_EndDate
            // 
            this.DE_EndDate.EditValue = null;
            this.DE_EndDate.Location = new System.Drawing.Point(11, 22);
            this.DE_EndDate.Name = "DE_EndDate";
            this.DE_EndDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_EndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_EndDate.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_EndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_EndDate.Properties.MaxValue = new System.DateTime(2019, 10, 26, 22, 15, 1, 953);
            this.DE_EndDate.Properties.ShowToday = false;
            this.DE_EndDate.Size = new System.Drawing.Size(132, 18);
            this.DE_EndDate.TabIndex = 0;
            this.DE_EndDate.TabStop = false;
            this.DE_EndDate.EditValueChanged += new System.EventHandler(this.DE_EndDate_EditValueChanged);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::SICGang.Properties.Resources.TB;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Controls.Add(this.DE_StartDate);
            this.panel2.Location = new System.Drawing.Point(149, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(154, 47);
            this.panel2.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Ngày bắt đầu";
            // 
            // DE_StartDate
            // 
            this.DE_StartDate.EditValue = null;
            this.DE_StartDate.Location = new System.Drawing.Point(11, 22);
            this.DE_StartDate.Name = "DE_StartDate";
            this.DE_StartDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_StartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_StartDate.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_StartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_StartDate.Properties.ShowToday = false;
            this.DE_StartDate.Size = new System.Drawing.Size(132, 18);
            this.DE_StartDate.TabIndex = 0;
            this.DE_StartDate.TabStop = false;
            // 
            // BT_Export
            // 
            this.BT_Export.Enabled = false;
            this.BT_Export.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Export.Location = new System.Drawing.Point(398, 81);
            this.BT_Export.Name = "BT_Export";
            this.BT_Export.Size = new System.Drawing.Size(105, 30);
            this.BT_Export.TabIndex = 1;
            this.BT_Export.TabStop = false;
            this.BT_Export.Text = "Export Excelfile";
            this.BT_Export.UseVisualStyleBackColor = true;
            this.BT_Export.Click += new System.EventHandler(this.BT_Export_Click);
            // 
            // BT_ViewAccessHistory
            // 
            this.BT_ViewAccessHistory.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_ViewAccessHistory.Location = new System.Drawing.Point(272, 81);
            this.BT_ViewAccessHistory.Name = "BT_ViewAccessHistory";
            this.BT_ViewAccessHistory.Size = new System.Drawing.Size(105, 30);
            this.BT_ViewAccessHistory.TabIndex = 1;
            this.BT_ViewAccessHistory.TabStop = false;
            this.BT_ViewAccessHistory.Text = "Access History";
            this.BT_ViewAccessHistory.UseVisualStyleBackColor = true;
            this.BT_ViewAccessHistory.Click += new System.EventHandler(this.BT_ViewAccessHistory_Click);
            // 
            // BT_ViewAdminDatalogs
            // 
            this.BT_ViewAdminDatalogs.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_ViewAdminDatalogs.Location = new System.Drawing.Point(149, 81);
            this.BT_ViewAdminDatalogs.Name = "BT_ViewAdminDatalogs";
            this.BT_ViewAdminDatalogs.Size = new System.Drawing.Size(105, 30);
            this.BT_ViewAdminDatalogs.TabIndex = 1;
            this.BT_ViewAdminDatalogs.TabStop = false;
            this.BT_ViewAdminDatalogs.Tag = "1";
            this.BT_ViewAdminDatalogs.Text = "Data Logs";
            this.BT_ViewAdminDatalogs.UseVisualStyleBackColor = true;
            this.BT_ViewAdminDatalogs.Click += new System.EventHandler(this.BT_ViewDatalogs_Click);
            // 
            // XSFD
            // 
            this.XSFD.FileName = "xtraSaveFileDialog1";
            this.XSFD.Filter = "Excel file|*.xls";
            // 
            // View
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.labelControl);
            this.Name = "View";
            this.Size = new System.Drawing.Size(800, 600);
            this.Load += new System.EventHandler(this.View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GC_View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_View)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DE_EndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_EndDate.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DE_StartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_StartDate.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraGrid.GridControl GC_View;
        private DevExpress.XtraGrid.Views.Grid.GridView GV_View;
        private System.Windows.Forms.Panel panel1;
        private CustomControls.MyButton BT_ViewAccessHistory;
        private CustomControls.MyButton BT_ViewAdminDatalogs;
        private DevExpress.XtraEditors.DateEdit DE_StartDate;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit DE_EndDate;

        #region Custom
        public View() => InitializeComponent();

        private void View_Load(object sender, EventArgs e)
        {
            DE_StartDate.Properties.MaxValue = DateTime.Now;
            DE_EndDate.Properties.MaxValue = DateTime.Now;

            DE_StartDate.EditValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DE_EndDate.EditValue = DateTime.Now;
        }

        private void DE_EndDate_EditValueChanged(object sender, EventArgs e) => DE_StartDate.Properties.MaxValue = Convert.ToDateTime(DE_EndDate.EditValue);
        #endregion
        private DevExpress.XtraEditors.XtraSaveFileDialog XSFD;
        private CustomControls.MyButton BT_Export;
    }
}