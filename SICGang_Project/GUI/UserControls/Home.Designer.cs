﻿using SICGang.Models;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SICGang.GUI.UserControls
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        ///
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.labelControl = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LB_AmountPeopleInRoom = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BT_AddClass = new CustomControls.BT_Add();
            this.BT_AddFaculty = new CustomControls.BT_Add();
            this.CBE_Classes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CBE_Faculties = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TE_Name = new DevExpress.XtraEditors.TextEdit();
            this.TE_StudentID = new DevExpress.XtraEditors.TextEdit();
            this.DE_DateOfBirth = new DevExpress.XtraEditors.DateEdit();
            this.PE_IMGStudent = new DevExpress.XtraEditors.PictureEdit();
            this.GC_PeopleInRoom = new DevExpress.XtraGrid.GridControl();
            this.GV_PeopleInRoom = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMãSV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHọtên = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgàysinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLớp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.WUIBP = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.XOFD = new DevExpress.XtraEditors.XtraOpenFileDialog(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Classes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_StudentID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PE_IMGStudent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC_PeopleInRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_PeopleInRoom)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl
            // 
            this.labelControl.AllowHtmlString = true;
            this.labelControl.Appearance.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl.Appearance.Options.UseFont = true;
            this.labelControl.Appearance.Options.UseForeColor = true;
            this.labelControl.Appearance.Options.UseTextOptions = true;
            this.labelControl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl.LineLocation = DevExpress.XtraEditors.LineLocation.Left;
            this.labelControl.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl.LineVisible = true;
            this.labelControl.Location = new System.Drawing.Point(0, 0);
            this.labelControl.Name = "labelControl";
            this.labelControl.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.labelControl.Size = new System.Drawing.Size(790, 30);
            this.labelControl.TabIndex = 1;
            this.labelControl.Text = "Home";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.GC_PeopleInRoom, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(790, 565);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LB_AmountPeopleInRoom);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.PE_IMGStudent);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 204);
            this.panel1.TabIndex = 0;
            // 
            // LB_AmountPeopleInRoom
            // 
            this.LB_AmountPeopleInRoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LB_AmountPeopleInRoom.AutoSize = true;
            this.LB_AmountPeopleInRoom.Location = new System.Drawing.Point(657, 188);
            this.LB_AmountPeopleInRoom.Name = "LB_AmountPeopleInRoom";
            this.LB_AmountPeopleInRoom.Size = new System.Drawing.Size(124, 13);
            this.LB_AmountPeopleInRoom.TabIndex = 2;
            this.LB_AmountPeopleInRoom.Text = "Số người trong phòng: 0";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::SICGang.Properties.Resources.FeildStudent;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.BT_AddClass);
            this.panel2.Controls.Add(this.BT_AddFaculty);
            this.panel2.Controls.Add(this.CBE_Classes);
            this.panel2.Controls.Add(this.CBE_Faculties);
            this.panel2.Controls.Add(this.labelControl4);
            this.panel2.Controls.Add(this.labelControl5);
            this.panel2.Controls.Add(this.labelControl3);
            this.panel2.Controls.Add(this.labelControl2);
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Controls.Add(this.TE_Name);
            this.panel2.Controls.Add(this.TE_StudentID);
            this.panel2.Controls.Add(this.DE_DateOfBirth);
            this.panel2.Location = new System.Drawing.Point(290, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(413, 150);
            this.panel2.TabIndex = 1;
            // 
            // BT_AddClass
            // 
            this.BT_AddClass.BackColor = System.Drawing.Color.Transparent;
            this.BT_AddClass.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BT_AddClass.BackgroundImage")));
            this.BT_AddClass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_AddClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_AddClass.ForeColor = System.Drawing.Color.White;
            this.BT_AddClass.Location = new System.Drawing.Point(362, 121);
            this.BT_AddClass.Name = "BT_AddClass";
            this.BT_AddClass.Size = new System.Drawing.Size(20, 20);
            this.BT_AddClass.TabIndex = 5;
            this.BT_AddClass.TabStop = false;
            this.BT_AddClass.UseVisualStyleBackColor = true;
            this.BT_AddClass.Visible = false;
            this.BT_AddClass.Click += new System.EventHandler(this.BT_AddClass_Click);
            // 
            // BT_AddFaculty
            // 
            this.BT_AddFaculty.BackColor = System.Drawing.Color.Transparent;
            this.BT_AddFaculty.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BT_AddFaculty.BackgroundImage")));
            this.BT_AddFaculty.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_AddFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_AddFaculty.ForeColor = System.Drawing.Color.White;
            this.BT_AddFaculty.Location = new System.Drawing.Point(362, 74);
            this.BT_AddFaculty.Name = "BT_AddFaculty";
            this.BT_AddFaculty.Size = new System.Drawing.Size(20, 20);
            this.BT_AddFaculty.TabIndex = 5;
            this.BT_AddFaculty.TabStop = false;
            this.BT_AddFaculty.UseVisualStyleBackColor = true;
            this.BT_AddFaculty.Visible = false;
            this.BT_AddFaculty.Click += new System.EventHandler(this.BT_AddFaculty_Click);
            // 
            // CBE_Classes
            // 
            this.CBE_Classes.Location = new System.Drawing.Point(194, 123);
            this.CBE_Classes.Name = "CBE_Classes";
            this.CBE_Classes.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Classes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Classes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Classes.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CBE_Classes.Properties.Sorted = true;
            this.CBE_Classes.Size = new System.Drawing.Size(162, 18);
            this.CBE_Classes.TabIndex = 4;
            this.CBE_Classes.TextChanged += new System.EventHandler(this.CBE_Classes_TextChanged);
            this.CBE_Classes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputSpace);
            // 
            // CBE_Faculties
            // 
            this.CBE_Faculties.Location = new System.Drawing.Point(194, 75);
            this.CBE_Faculties.Name = "CBE_Faculties";
            this.CBE_Faculties.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Faculties.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Faculties.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Faculties.Properties.Sorted = true;
            this.CBE_Faculties.Size = new System.Drawing.Size(162, 18);
            this.CBE_Faculties.TabIndex = 3;
            this.CBE_Faculties.SelectedIndexChanged += new System.EventHandler(this.CBE_Faculties_SelectedIndexChanged);
            this.CBE_Faculties.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputDigit);
            this.CBE_Faculties.Leave += new System.EventHandler(this.CBE_Faculties_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(194, 104);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(17, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Lớp";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(194, 57);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Khoa";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(47, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Ngày sinh";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Họ tên";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Mã SV";
            // 
            // TE_Name
            // 
            this.TE_Name.Location = new System.Drawing.Point(11, 75);
            this.TE_Name.Name = "TE_Name";
            this.TE_Name.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_Name.Size = new System.Drawing.Size(162, 18);
            this.TE_Name.TabIndex = 1;
            this.TE_Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputDigit);
            this.TE_Name.Leave += new System.EventHandler(this.TE_Name_Leave);
            // 
            // TE_StudentID
            // 
            this.TE_StudentID.Location = new System.Drawing.Point(11, 28);
            this.TE_StudentID.Name = "TE_StudentID";
            this.TE_StudentID.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.TE_StudentID.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.TE_StudentID.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_StudentID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_StudentID.Properties.MaxLength = 10;
            this.TE_StudentID.Size = new System.Drawing.Size(162, 18);
            this.TE_StudentID.TabIndex = 0;
            this.TE_StudentID.EnabledChanged += new System.EventHandler(this.TE_StudentID_EnabledChanged);
            this.TE_StudentID.TextChanged += new System.EventHandler(this.TE_StudentID_TextChanged);
            this.TE_StudentID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TE_StudentID_KeyDown);
            this.TE_StudentID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TE_StudentID_KeyPress);
            // 
            // DE_DateOfBirth
            // 
            this.DE_DateOfBirth.EditValue = null;
            this.DE_DateOfBirth.Location = new System.Drawing.Point(11, 123);
            this.DE_DateOfBirth.Name = "DE_DateOfBirth";
            this.DE_DateOfBirth.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_DateOfBirth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_DateOfBirth.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_DateOfBirth.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_DateOfBirth.Properties.ShowToday = false;
            this.DE_DateOfBirth.Size = new System.Drawing.Size(162, 18);
            this.DE_DateOfBirth.TabIndex = 2;
            // 
            // PE_IMGStudent
            // 
            this.PE_IMGStudent.Location = new System.Drawing.Point(120, 35);
            this.PE_IMGStudent.Name = "PE_IMGStudent";
            this.PE_IMGStudent.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.PE_IMGStudent.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.PE_IMGStudent.Size = new System.Drawing.Size(130, 150);
            this.PE_IMGStudent.TabIndex = 0;
            this.PE_IMGStudent.ImageChanged += new System.EventHandler(this.PE_IMGStudent_ImageChanged);
            this.PE_IMGStudent.Click += new System.EventHandler(this.PE_IMGStudent_Click);
            // 
            // GC_PeopleInRoom
            // 
            this.GC_PeopleInRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC_PeopleInRoom.Location = new System.Drawing.Point(3, 213);
            this.GC_PeopleInRoom.MainView = this.GV_PeopleInRoom;
            this.GC_PeopleInRoom.Name = "GC_PeopleInRoom";
            this.GC_PeopleInRoom.Size = new System.Drawing.Size(784, 293);
            this.GC_PeopleInRoom.TabIndex = 1;
            this.GC_PeopleInRoom.TabStop = false;
            this.GC_PeopleInRoom.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV_PeopleInRoom});
            // 
            // GV_PeopleInRoom
            // 
            this.GV_PeopleInRoom.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMãSV,
            this.colHọtên,
            this.colNgàysinh,
            this.colLớp,
            this.colKhoa});
            this.GV_PeopleInRoom.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GV_PeopleInRoom.GridControl = this.GC_PeopleInRoom;
            this.GV_PeopleInRoom.Name = "GV_PeopleInRoom";
            this.GV_PeopleInRoom.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV_PeopleInRoom.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV_PeopleInRoom.OptionsBehavior.Editable = false;
            this.GV_PeopleInRoom.OptionsView.ShowGroupPanel = false;
            this.GV_PeopleInRoom.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.GV_PeopleInRoom_RowClick);
            // 
            // colMãSV
            // 
            this.colMãSV.FieldName = "Mã SV";
            this.colMãSV.Name = "colMãSV";
            this.colMãSV.Visible = true;
            this.colMãSV.VisibleIndex = 0;
            // 
            // colHọtên
            // 
            this.colHọtên.FieldName = "Họ tên";
            this.colHọtên.Name = "colHọtên";
            this.colHọtên.Visible = true;
            this.colHọtên.VisibleIndex = 1;
            // 
            // colNgàysinh
            // 
            this.colNgàysinh.FieldName = "Ngày sinh";
            this.colNgàysinh.Name = "colNgàysinh";
            this.colNgàysinh.Visible = true;
            this.colNgàysinh.VisibleIndex = 2;
            // 
            // colLớp
            // 
            this.colLớp.FieldName = "Lớp";
            this.colLớp.Name = "colLớp";
            this.colLớp.Visible = true;
            this.colLớp.VisibleIndex = 3;
            // 
            // colKhoa
            // 
            this.colKhoa.FieldName = "Khoa";
            this.colKhoa.Name = "colKhoa";
            this.colKhoa.Visible = true;
            this.colKhoa.VisibleIndex = 4;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.WUIBP);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 509);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(790, 56);
            this.panel3.TabIndex = 2;
            // 
            // WUIBP
            // 
            this.WUIBP.AllowGlyphSkinning = false;
            this.WUIBP.BackColor = System.Drawing.Color.Gainsboro;
            this.WUIBP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WUIBP.ButtonInterval = 35;
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            this.WUIBP.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Save", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Repair", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, false, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Reset Field", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Delete", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, false, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Exit", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, false, null, true, false, true, null, -1, false)});
            this.WUIBP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WUIBP.EnableImageTransparency = true;
            this.WUIBP.Location = new System.Drawing.Point(0, 0);
            this.WUIBP.Name = "WUIBP";
            this.WUIBP.Size = new System.Drawing.Size(790, 56);
            this.WUIBP.TabIndex = 0;
            this.WUIBP.TabStop = false;
            this.WUIBP.UseButtonBackgroundImages = false;
            this.WUIBP.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.WUIBP_ButtonClick);
            // 
            // XOFD
            // 
            this.XOFD.Filter = "JPeg Image|*.jpg|PNG Image|*.png|Bitmap Image|*.bmp";
            // 
            // Home
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.labelControl);
            this.Name = "Home";
            this.Size = new System.Drawing.Size(790, 595);
            this.Load += new System.EventHandler(this.XUC_Home_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Classes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_StudentID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PE_IMGStudent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC_PeopleInRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_PeopleInRoom)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.PictureEdit PE_IMGStudent;
        private DevExpress.XtraGrid.GridControl GC_PeopleInRoom;
        private DevExpress.XtraGrid.Views.Grid.GridView GV_PeopleInRoom;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.DateEdit DE_DateOfBirth;
        private DevExpress.XtraEditors.TextEdit TE_Name;
        private DevExpress.XtraEditors.TextEdit TE_StudentID;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Classes;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Faculties;
        private DevExpress.XtraGrid.Columns.GridColumn colMãSV;
        private DevExpress.XtraGrid.Columns.GridColumn colHọtên;
        private DevExpress.XtraGrid.Columns.GridColumn colNgàysinh;
        private DevExpress.XtraGrid.Columns.GridColumn colLớp;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoa;
        private Panel panel3;
        private DevExpress.XtraEditors.XtraOpenFileDialog XOFD;
        private CustomControls.BT_Add BT_AddFaculty;
        private CustomControls.BT_Add BT_AddClass;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel WUIBP;

        #region Custom
        /// <summary>
        /// Information of Student selected in Gridview.
        /// </summary>
        private Students StudentOld = null;
        private Accounts UserAccount = null;
        public Accounts SetUserAccount { set => UserAccount = value; }
        public NotifyIcon Notify = null;
        public Home() => InitializeComponent();

        #region Textbox
        private void TE_Name_Leave(object sender, EventArgs e)
        {
            TE_Name.Text = MyHelper.TitleFormat(TE_Name.Text);
        }

        private void NotAllowInputDigit(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void NotAllowInputSpace(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
                e.Handled = true;
        }
        #endregion

        #region Gridview select row
        private void GV_PeopleInRoom_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            TE_StudentID.Text = GV_PeopleInRoom.GetFocusedDataRow().ItemArray[0].ToString();

            TE_StudentID.Enabled = false;

            StudentOld = GetStudentFromField();
        }

        private void ExportDataToField(Students _Student)
        {
            try
            {
                ExportDataToField(_Student.GetStudentID(), _Student.GetName(), _Student.GetDateOfBirthString(), _Student.GetFacultyName(), _Student.GetClassName());
            }
            catch { ExportDataToField(_Student.GetStudentID(), "", "", "", ""); }
        }

        private void ExportDataToField(string _StudentID, string _StudentName, string _DateOfBirth, string _FacultyName, string _ClassName)
        {
            TE_StudentID.Text = _StudentID;
            TE_Name.Text = _StudentName;
            DE_DateOfBirth.Text = _DateOfBirth.Split(' ')[0];
            CBE_Faculties.Text = _FacultyName;
            CBE_Classes.Text = _ClassName;
        }

        private void ExportDataToField(string _StudentID, string _StudentName, string _DateOfBirth, string _FacultyName, string _ClassName, Image _Img)
        {
            TE_StudentID.Text = _StudentID;
            TE_Name.Text = _StudentName;
            DE_DateOfBirth.Text = _DateOfBirth.Split(' ')[0];
            CBE_Faculties.Text = _FacultyName;
            CBE_Classes.Text = _ClassName;
            PE_IMGStudent.Image = _Img;
        }
        #endregion

        #region Event on field
        private void PE_IMGStudent_ImageChanged(object sender, EventArgs e)
        {
            if (PE_IMGStudent.Image != null)
            {
                PE_IMGStudent.Size = new Size()
                {
                    Height = PE_IMGStudent.Size.Height,
                    Width = (PE_IMGStudent.Image.Size.Width * PE_IMGStudent.Size.Height) / PE_IMGStudent.Image.Size.Height
                };
            }
            else PE_IMGStudent.Size = new Size() { Width = 130, Height = 150 };

            IMGisOpen = false;
        }

        #endregion

        private bool IMGisOpen = false;
        private void PE_IMGStudent_Click(object sender, EventArgs e)
        {
            if (XOFD.ShowDialog() == DialogResult.OK)
            {
                PE_IMGStudent.Image = Image.FromFile(XOFD.FileName);
                IMGisOpen = true;
            }

            TE_StudentID.Select();
        }

        private Students GetStudentFromField()
        {
            if (TE_StudentID.Text.Length < 10) return null;

            if (string.IsNullOrEmpty(TE_StudentID.Text) || string.IsNullOrEmpty(TE_Name.Text) || string.IsNullOrEmpty(DE_DateOfBirth.Text) || string.IsNullOrEmpty(CBE_Classes.Text)) return null;
            else
            {
                var Student = new Students();

                Student.SetStudentID(TE_StudentID.Text);
                Student.SetName(TE_Name.Text);
                Student.SetDateOfBirth(Convert.ToDateTime(DE_DateOfBirth.EditValue));
                Student.SetClassID(Classes.Get(CBE_Classes.Text).GetClassID());

                return Student;
            }
        }

        private void ResetField()
        {
            TE_StudentID.Enabled = true;

            IMGisOpen = false;

            ExportDataToField("", "", "", "", "");

            TE_StudentID.Select();

            PE_IMGStudent.Image = null;

            StudentOld = null;
        }

        private void TE_StudentID_EnabledChanged(object sender, EventArgs e)
        {
            if (TE_StudentID.Enabled)
            {
                WUIBP.Buttons[0].Properties.Enabled = true;

                WUIBP.Buttons[1].Properties.Enabled = false;
                WUIBP.Buttons[4].Properties.Enabled = false;
                WUIBP.Buttons[5].Properties.Enabled = false;
            }
            else
            {
                WUIBP.Buttons[0].Properties.Enabled = false;

                WUIBP.Buttons[1].Properties.Enabled = true;
                WUIBP.Buttons[4].Properties.Enabled = true;
                WUIBP.Buttons[5].Properties.Enabled = true;
            }
        }
        #endregion
        private Label LB_AmountPeopleInRoom;
    }
}