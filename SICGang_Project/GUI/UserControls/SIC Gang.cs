﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using SICGang.Models;

namespace SICGang.GUI.UserControls
{
    public partial class SICGang : XtraUserControl
    {
        #region Load
        private void SICGang_Load(object sender, EventArgs e)
        {
            FIllData_GC_Accounts();

            FillData_CBE_Faculties();

            Notify = (this.ParentForm as Main).Notify;

            DE_DateOfBirth.Properties.MinValue = new DateTime(DateTime.Now.Year - 28, 1, 1);
            DE_DateOfBirth.Properties.MaxValue = new DateTime(DateTime.Now.Year - 18, 12, DateTime.DaysInMonth(DateTime.Now.Year - 18, 12));
        }

        private void FIllData_GC_Accounts()
        {
            GC_Accounts.DataSource = SQL_Sever.ExecuteDataTable("select * from view_Accounts");
        }

        private void FillData_CBE_Classes()
        {
            CBE_Classes.Properties.Items.Clear();
            var ClassNames = Faculties.GetClassNames(CBE_Faculties.Text);
            CBE_Classes.Properties.Items.AddRange(ClassNames);
        }

        private void FillData_CBE_Faculties()
        {
            CBE_Faculties.Properties.Items.Clear();
            CBE_Faculties.Properties.Items.AddRange(Faculties.GetFacultyNames());
        }
        #endregion

        private void TE_StudentID_TextChanged(object sender, EventArgs e)
        {
            if (TE_StudentID.Text.Length == 10)
            {
                string StudentID = TE_StudentID.Text;

                try
                {
                    var Student = Students.Get(StudentID);

                    ExportDataToField(Student);

                    try { if (!IMGisOpen) PE_IMGStudent.Image = Student.GetIMG(); }
                    catch { PE_IMGStudent.Image = null; }
                }
                catch { ExportDataToField(StudentID, "", "", "", "", null); }

                if (PE_IMGStudent.Image == null)
                {
                    string StudentIMG = $"./studentIMGs/{StudentID}.jpg";

                    if (File.Exists(StudentIMG))
                        PE_IMGStudent.Image = Image.FromFile(StudentIMG);
                }
            }
            else
            {
                if (!IMGisOpen)
                    PE_IMGStudent.Image = null;
            }
        }

        #region CBE Faculties Classes
        private void CBE_Faculties_Leave(object sender, EventArgs e)
        {
            try
            {
                var Student = Students.Get(TE_StudentID.Text);
                CBE_Classes.Text = Student.GetFacultyName() != CBE_Faculties.Text ? "" : CBE_Classes.Text = Student.GetClassName();
            }
            catch { CBE_Classes.Text = ""; }

            if (CBE_Faculties.SelectedIndex != -1)
            {
                CBE_Classes.Properties.Items.Clear();
                BT_AddFaculty.Hide();

                var ClassNames = Faculties.GetClassNames(CBE_Faculties.Text);

                if (ClassNames.Count != 0)
                {
                    BT_AddClass.Hide();
                    CBE_Classes.Properties.Items.AddRange(ClassNames);
                }
                else BT_AddClass.Show();
            }
            else
            {
                CBE_Faculties.Text = MyHelper.TitleFormat(CBE_Faculties.Text);

                if (string.IsNullOrEmpty(CBE_Faculties.Text))
                {
                    BT_AddFaculty.Hide();
                    BT_AddClass.Hide();
                }
                else
                {
                    BT_AddFaculty.Show();
                    BT_AddClass.Show();
                }
            }
        }

        private void CBE_Faculties_SelectedIndexChanged(object sender, EventArgs e)
        {
            BT_AddFaculty.Visible = (CBE_Faculties.SelectedIndex == -1 && !string.IsNullOrEmpty(CBE_Faculties.Text));

            FillData_CBE_Classes();
        }

        private void CBE_Classes_TextChanged(object sender, EventArgs e)
        {
            BT_AddClass.Visible = (CBE_Classes.SelectedIndex == -1 && !string.IsNullOrEmpty(CBE_Classes.Text));
        }
        #endregion

        private void BT_Add_Click(object sender, EventArgs e)
        {
            if (GetStudentFromField() != null)
            {
                SaveStudent();

                SaveIMG();

                SaveAccount();

                FIllData_GC_Accounts();
            }
            else (this.ParentForm as Main).Notify.ShowBalloonTip(3000, "Warning", "Thông tin sinh viên bị thiếu!", ToolTipIcon.Warning);
        }

        private void SaveAccount()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now.AddSeconds(1), "");

            var Account = new Accounts();
            Account.SetStudentID(TE_StudentID.Text);
            Account.SetUser(TE_StudentID.Text + $"@{(RB_Admin.Checked ? "admin" : "user")}");
            Account.SetPassword(MyHelper.CreatePasswordHash(TE_StudentID.Text, Account.GetUser()));
            Account.SetIsAdmin(RB_Admin.Checked);

            try
            {
                Account.Post();
                Datalog.SetModifiedContent($"Cấp quyền {(Account.GetIsAdmin() ? "admin" : "truy cập")} cho {TE_Name.Text}");
                Datalog.Post();
            }
            catch { Notify.ShowBalloonTip(3000, "Error", "Tạo mới tài khoản không thành công!", ToolTipIcon.Error); }
        }

        private void BT_Repair_Click(object sender, EventArgs e)
        {
            if (StudentOld != null)
            {
                if (GetStudentFromField() != null)
                {
                    SaveStudent();

                    SaveIMG();

                    RepairAccount();

                    FIllData_GC_Accounts();

                    //Notify.ShowBalloonTip(3000, "Kết quả", "Đã sửa tài khoản thành công!", ToolTipIcon.Info);
                }
                else Notify.ShowBalloonTip(3000, "Warning", "Thông tin sinh viên bị thiếu!", ToolTipIcon.Warning);
            }
        }

        private void RepairAccount()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now.AddSeconds(1), "");

            if (AccountOld.GetIsAdmin() != RB_Admin.Checked)
            {
                AccountOld.SetIsAdmin(RB_Admin.Checked);
                if (!AccountOld.Update()) Notify.ShowBalloonTip(3000, "Error", "Sửa tài khoản không thành công!", ToolTipIcon.Error);
                else
                {
                    Notify.ShowBalloonTip(3000, "Kết quả", "Đã sửa tài khoản thành công!", ToolTipIcon.Info);

                    Datalog.SetModifiedContent($"Sửa tài khoản {(!AccountOld.GetIsAdmin() ? "admin" : "user")} của {StudentOld.GetName()} thành {(AccountOld.GetIsAdmin() ? "admin" : "user")}");
                    Datalog.Post();

                    BT_Repair.Enabled = false;
                    BT_Delete.Enabled = false;
                }
            }
        }

        private void BT_Delete_Click(object sender, EventArgs e)
        {
            if (StudentOld != null)
            {
                if (AccountOld.GetUser() != UserAccount.GetUser())
                {
                    DeleteAccount();

                    FIllData_GC_Accounts();

                    Notify.ShowBalloonTip(3000, "Kết quả", "Đã xóa tài khoản thành công!", ToolTipIcon.Info);
                }
                else MessageBox.Show("Không thể xóa tài khoản của chính mình!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DeleteAccount()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");

            if (!AccountOld.Delete())
                MessageBox.Show("Xóa tài khoản lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                Datalog.SetModifiedContent($"Xóa tài khoản {(AccountOld.GetIsAdmin() ? "admin" : "user")} của {StudentOld.GetName()}");
                Datalog.Post();

                BT_Repair.Enabled = false;
                BT_Delete.Enabled = false;
            }
        }

        private void SaveStudent()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var StudentInput = GetStudentFromField();

            try
            {
                var Student = Students.Get(StudentInput.GetStudentID());

                if (!StudentInput.Equals(Student))
                    if (!StudentInput.Update())
                        MessageBox.Show("Cập nhận thông tin sinh viên lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        Datalog.SetModifiedContent($"Sửa thông tin sinh viên {StudentInput.GetStudentID()}");
                        Datalog.Post();
                    }
            }
            catch
            {
                if (!StudentInput.Post())
                    MessageBox.Show("Thêm thông tin sinh viên lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Datalog.SetModifiedContent($"Thêm thông tin sinh viên {StudentInput.GetStudentID()}");
                    Datalog.Post();
                }
            }
        }

        private void SaveIMG()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var StudentIMG = new StudentIMGs();
            StudentIMG.SetStudentID(TE_StudentID.Text);

            try
            {
                StudentIMGs.Get(StudentIMG.GetStudentID());

                if (IMGisOpen)
                {
                    StudentIMG.SetIMG(PE_IMGStudent.Image);
                    StudentIMG.Update();
                    IMGisOpen = false;

                    Datalog.SetModifiedContent($"Sửa ảnh sinh viên {StudentIMG.GetStudentID()}");
                    Datalog.Post();
                }
            }
            catch
            {
                if (PE_IMGStudent.Image != null)
                {
                    StudentIMG.SetIMG(PE_IMGStudent.Image);
                    StudentIMG.Post();
                    IMGisOpen = false;
                }
            }
        }

        private void BT_Reset_Click(object sender, EventArgs e)
        {
            TE_StudentID.Enabled = true;

            IMGisOpen = false;

            TE_StudentID.Select();

            ExportDataToField("", "", "", "", "");
            RB_User.Checked = true;

            PE_IMGStudent.Image = null;

            StudentOld = null;
            AccountOld = null;

            BT_Repair.Enabled = false;
            BT_Delete.Enabled = false;
        }

        #region BT Add
        private void BT_AddFaculty_Click(object sender, EventArgs e)
        {
            var Faculty = new Faculties();
            Faculty.SetName(CBE_Faculties.Text);

            if (MessageBox.Show($"Xác nhận thêm khoa {Faculty.GetName()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (Faculty.Post())
                {
                    BT_AddFaculty.Hide();
                    FillData_CBE_Faculties();
                    MessageBox.Show($"Đã thêm khoa {Faculty.GetName()} thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show($"Thêm khoa {Faculty.GetName()} lỗi!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            CBE_Faculties.Select();
        }

        private void BT_AddClass_Click(object sender, EventArgs e)
        {
            var Class = new Classes();
            Class.SetName(CBE_Classes.Text);
            Class.SetFacultyID(Faculties.Get(CBE_Faculties.Text).GetFacultyID());

            if (MessageBox.Show($"Xác nhận thêm lớp {Class.GetName()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (Class.Post())
                {
                    BT_AddClass.Hide();
                    FillData_CBE_Classes();
                    MessageBox.Show($"Đã thêm lớp {Class.GetName()} thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show($"Thêm lớp {Class.GetName()} lỗi!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            CBE_Classes.Select();
        }
        #endregion
    }
}