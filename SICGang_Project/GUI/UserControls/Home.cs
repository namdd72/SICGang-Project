﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using SICGang.Models;
using DevExpress.XtraBars.Docking2010;
using System.Threading;
using System.Data;

namespace SICGang.GUI.UserControls
{
    public partial class Home : XtraUserControl
    {
        #region Load
        private void XUC_Home_Load(object sender, EventArgs e)
        {
            FillData_GC_PeopleInRoom();

            FillData_CBE_Faculties();

            Notify = (this.ParentForm as Main).Notify;

            DE_DateOfBirth.Properties.MinValue = new DateTime(DateTime.Now.Year - 28, 1, 1);
            DE_DateOfBirth.Properties.MaxValue = new DateTime(DateTime.Now.Year - 18, 12, DateTime.DaysInMonth(DateTime.Now.Year - 18, 12));

            TE_StudentID.Select();
        }

        private void FillData_GC_PeopleInRoom()
        {
            DataTable DataSuorce = SQL_Sever.ExecuteDataTable("select * from view_PeopleInRoomNow");
            GC_PeopleInRoom.DataSource = DataSuorce;
            LB_AmountPeopleInRoom.Text = $"Số người trong phòng: {DataSuorce.Rows.Count}";
        }

        private void FillData_CBE_Faculties()
        {
            CBE_Faculties.Properties.Items.Clear();
            CBE_Faculties.Properties.Items.AddRange(Faculties.GetFacultyNames());
        }

        private void FillData_CBE_Classes()
        {
            CBE_Classes.Properties.Items.Clear();
            var ClassNames = Faculties.GetClassNames(CBE_Faculties.Text);
            CBE_Classes.Properties.Items.AddRange(ClassNames);
        }
        #endregion

        private void TE_StudentID_TextChanged(object sender, EventArgs e)
        {
            if (TE_StudentID.Text.Length == 10)
            {
                string StudentID = TE_StudentID.Text;

                try
                {
                    var Student = Students.Get(StudentID);

                    ExportDataToField(Student);

                    try { if (!IMGisOpen) PE_IMGStudent.Image = Student.GetIMG(); }
                    catch { PE_IMGStudent.Image = null; }
                }
                catch { ExportDataToField(StudentID, "", "", "", "", null); }

                if (PE_IMGStudent.Image == null)
                {
                    string StudentIMG = $"./studentIMGs/{StudentID}.jpg";

                    if (File.Exists(StudentIMG))
                        PE_IMGStudent.Image = Image.FromFile(StudentIMG);
                }
            }
            else
            {
                if (!IMGisOpen)
                    PE_IMGStudent.Image = null;
            }
        }

        #region CBE Faculties Classes
        private void CBE_Classes_TextChanged(object sender, EventArgs e)
        {
            BT_AddClass.Visible = (CBE_Classes.SelectedIndex == -1 && !string.IsNullOrEmpty(CBE_Classes.Text));
        }

        private void CBE_Faculties_SelectedIndexChanged(object sender, EventArgs e)
        {
            BT_AddFaculty.Visible = (CBE_Faculties.SelectedIndex == -1 && !string.IsNullOrEmpty(CBE_Faculties.Text));

            FillData_CBE_Classes();
        }

        private void CBE_Faculties_Leave(object sender, EventArgs e)
        {
            try
            {
                var Student = Students.Get(TE_StudentID.Text);
                CBE_Classes.Text = Student.GetFacultyName() != CBE_Faculties.Text ? "" : CBE_Classes.Text = Student.GetClassName();
            }
            catch { CBE_Classes.Text = ""; }

            if (CBE_Faculties.SelectedIndex != -1)
            {
                CBE_Classes.Properties.Items.Clear();
                BT_AddFaculty.Hide();

                var ClassNames = Faculties.GetClassNames(CBE_Faculties.Text);

                if (ClassNames.Count != 0)
                {
                    BT_AddClass.Hide();
                    CBE_Classes.Properties.Items.AddRange(ClassNames);
                }
                else BT_AddClass.Show();
            }
            else
            {
                CBE_Faculties.Text = MyHelper.TitleFormat(CBE_Faculties.Text);

                if (string.IsNullOrEmpty(CBE_Faculties.Text))
                {
                    BT_AddFaculty.Hide();
                    BT_AddClass.Hide();
                }
                else
                {
                    BT_AddFaculty.Show();
                    BT_AddClass.Show();
                }
            }
        }

        #region BT Add
        private void BT_AddFaculty_Click(object sender, EventArgs e)
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var Faculty = new Faculties();
            Faculty.SetName(CBE_Faculties.Text);

            if (MessageBox.Show($"Xác nhận thêm khoa {Faculty.GetName()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (Faculty.Post())
                {
                    BT_AddFaculty.Hide();
                    FillData_CBE_Faculties();
                    Datalog.SetModifiedContent($"Thêm khoa {Faculty.GetName()}");
                    Datalog.Post();
                    MessageBox.Show($"Đã thêm khoa {Faculty.GetName()} thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show($"Thêm khoa {Faculty.GetName()} lỗi!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            CBE_Faculties.Select();
        }

        private void BT_AddClass_Click(object sender, EventArgs e)
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var Class = new Classes();
            Class.SetName(CBE_Classes.Text);
            Class.SetFacultyID(Faculties.Get(CBE_Faculties.Text).GetFacultyID());

            if (MessageBox.Show($"Xác nhận thêm lớp {Class.GetName()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (Class.Post())
                {
                    BT_AddClass.Hide();
                    FillData_CBE_Classes();
                    Datalog.SetModifiedContent($"Thêm lớp {Class.GetName()}");
                    Datalog.Post();
                    MessageBox.Show($"Đã thêm lớp {Class.GetName()} thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show($"Thêm lớp {Class.GetName()} lỗi!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            CBE_Classes.Select();
        }
        #endregion

        #endregion

        private void WUIBP_ButtonClick(object sender, ButtonEventArgs e)
        {
            switch ((e.Button as WindowsUIButton).Caption)
            {
                case "Save":
                    if (StudentOld == null)
                    {
                        if (GetStudentFromField() != null)
                        {
                            if (UserAccount.GetStudentID() == TE_StudentID.Text)
                                Notify.ShowBalloonTip(3000, "Warning", "Bạn đang ở trong phòng!", ToolTipIcon.Warning);
                            else if (AccessHistories.CkeckStudentInRoom(TE_StudentID.Text))
                                Notify.ShowBalloonTip(3000, "Warning", $"Sinh viên {TE_StudentID.Text} vẫn đang trong phòng!", ToolTipIcon.Warning);
                            else
                            {
                                SaveStudent();

                                SaveIMG();

                                SaveAccessHistory();

                                FillData_GC_PeopleInRoom();

                                TE_StudentID.Select();
                            }
                        }
                        else Notify.ShowBalloonTip(3000, "Warning", "Thông tin sinh viên bị thiếu!", ToolTipIcon.Warning);
                    }
                    break;
                case "Repair":
                    if (StudentOld != null)
                    {
                        if (GetStudentFromField() != null)
                        {
                            SaveStudent();

                            SaveIMG();

                            FillData_GC_PeopleInRoom();

                            Notify.ShowBalloonTip(3000, "Kết quả", "Đã sửa thông tin sinh viên thành công!", ToolTipIcon.Info);
                        }
                        else Notify.ShowBalloonTip(3000, "Warning", "Thông tin sinh viên bị thiếu!", ToolTipIcon.Warning);
                    }
                    break;
                case "Reset Field":
                    ResetField();
                    break;
                case "Delete":
                    if (StudentOld != null)
                    {
                        if (MessageBox.Show($"Xác nhận xóa thông tin ra vào của sinh viên {StudentOld.GetStudentID()}?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        {
                            DeleteAccessHistory();

                            FillData_GC_PeopleInRoom();

                            Notify.ShowBalloonTip(3000, "Kết quả", "Đã xóa lịch sử vào phòng thành công!", ToolTipIcon.Info);
                        }
                    }
                    break;
                case "Exit":
                    if (StudentOld != null)
                    {
                        StudentExit();

                        FillData_GC_PeopleInRoom();

                        Notify.ShowBalloonTip(3000, "Kết quả", "Cập nhật trạng thái ra khỏi phòng thành công!", ToolTipIcon.Info);
                    }
                    break;
            }
        }

        private void SaveStudent()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var StudentInput = GetStudentFromField();

            try
            {
                var Student = Students.Get(StudentInput.GetStudentID());

                if (!StudentInput.Equals(Student))
                    if (!StudentInput.Update())
                        MessageBox.Show("Cập nhận thông tin sinh viên lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        Datalog.SetModifiedContent($"Sửa thông tin sinh viên {StudentInput.GetStudentID()}");
                        Datalog.Post();

                        StudentOld = null;
                    }
                else StudentOld = null;
            }
            catch
            {
                if (!StudentInput.Post())
                    MessageBox.Show("Thêm thông tin sinh viên lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Datalog.SetModifiedContent($"Thêm thông tin sinh viên {StudentInput.GetStudentID()}");
                    Datalog.Post();

                    StudentOld = null;
                }
            }
        }

        private void SaveIMG()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var StudentIMG = new StudentIMGs();
            StudentIMG.SetStudentID(TE_StudentID.Text);

            try
            {
                StudentIMGs.Get(StudentIMG.GetStudentID());

                if (IMGisOpen)
                {
                    StudentIMG.SetIMG(PE_IMGStudent.Image);
                    StudentIMG.Update();
                    IMGisOpen = false;

                    Datalog.SetModifiedContent($"Sửa ảnh sinh viên {StudentIMG.GetStudentID()}");
                    Datalog.Post();
                }
            }
            catch
            {
                if (PE_IMGStudent.Image != null)
                {
                    StudentIMG.SetIMG(PE_IMGStudent.Image);
                    StudentIMG.Post();
                    IMGisOpen = false;
                }
            }
        }

        private void SaveAccessHistory()
        {
            var AccessHistory = new AccessHistories();
            AccessHistory.SetStudentID(TE_StudentID.Text);
            AccessHistory.SetTimeEnter(DateTime.Now);

            if (!AccessHistory.Post())
                MessageBox.Show("Thêm lịch sử ra vào lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void DeleteAccessHistory()
        {
            var Datalog = new Datalogs(UserAccount.GetStudentID(), DateTime.Now, "");
            var AccessHistory = new AccessHistories();

            AccessHistory.SetStudentID(StudentOld.GetStudentID());

            if (!AccessHistory.Delete())
                MessageBox.Show("Xóa lịch sử ra vào lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                Datalog.SetModifiedContent($"Xóa lịch sử vào phòng sinh viên {AccessHistory.GetStudentID()}");
                Datalog.Post();

                StudentOld = null;
            }
        }

        private void StudentExit()
        {
            var AccessHistory = new AccessHistories();

            AccessHistory.SetStudentID(StudentOld.GetStudentID());
            AccessHistory.SetTimeLeave(DateTime.Now);

            if (!AccessHistory.Update())
                MessageBox.Show("Cập nhật trạng thái ra khỏi phòng lỗi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else StudentOld = null;
        }

        Scan scan = new Scan();
        private void TE_StudentID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (scan.GetText().Length == 10) TE_StudentID.Text = scan.GetText();
                else if (scan.GetText().Length > 10) ExportDataToField("", "", "", "", "", null);
                if (StudentOld == null)
                {
                    if (!AccessHistories.CkeckStudentInRoom(TE_StudentID.Text))
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(Students.Get(TE_StudentID.Text).GetName()))
                            {
                                if (GetStudentFromField() != null)
                                    WUIBP_ButtonClick(null, new ButtonEventArgs(WUIBP.Buttons[0] as IButton));
                            }
                            else ExportDataToField(TE_StudentID.Text, "", "", "", "", StudentIMGs.Get(TE_StudentID.Text).GetIMG());
                        }
                        catch { ExportDataToField(TE_StudentID.Text, "", "", "", "", null); }
                    }
                    else
                    {
                        StudentOld = GetStudentFromField();
                        WUIBP_ButtonClick(null, new ButtonEventArgs(WUIBP.Buttons[5] as IButton));
                    }
                }
            }
        }

        private void TE_StudentID_KeyPress(object sender, KeyPressEventArgs e)
        {
            NotAllowInputSpace(sender, e);
            if (Char.IsLetterOrDigit(e.KeyChar)) scan.AddText(e.KeyChar);
        }
    }
}