﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SICGang.Models;

namespace SICGang.GUI.UserControls
{
    public partial class View : XtraUserControl
    {
        private void BT_ViewDatalogs_Click(object sender, EventArgs e)
        {
            string Query = $"select * from dbo.func_GetDatalogs('{DE_StartDate.Text}', '{DE_EndDate.Text}')";

            GV_View.Columns.Clear();
            GC_View.DataSource = SQL_Sever.ExecuteDataTable(Query);

            GV_View.ClearSorting();
            GV_View.Columns[2].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;

            XSFD.FileName = $"Datalogs [{DE_StartDate.Text} {DE_EndDate.Text}]";
            BT_Export.Enabled = true;
        }

        private void BT_ViewAccessHistory_Click(object sender, EventArgs e)
        {
            string Query = $"select * from dbo.func_GetAccessHistories('{DE_StartDate.Text}', '{DE_EndDate.Text}')";

            GV_View.Columns.Clear();
            GC_View.DataSource = SQL_Sever.ExecuteDataTable(Query);

            GV_View.ClearSorting();
            GV_View.Columns[5].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;

            XSFD.FileName = $"AccessHistories [{DE_StartDate.Text} {DE_EndDate.Text}]";
            BT_Export.Enabled = true;
        }

        private void BT_Export_Click(object sender, EventArgs e)
        {
            if (XSFD.ShowDialog() == DialogResult.OK)
                GC_View.ExportToXls(XSFD.FileName);
        }
    }
}