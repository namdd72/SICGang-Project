﻿using SICGang.Models;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI.UserControls
{
    partial class SICGang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        ///
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.labelControl = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GC_Accounts = new DevExpress.XtraGrid.GridControl();
            this.GV_Accounts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BT_Delete = new CustomControls.MyButton();
            this.BT_Repair = new CustomControls.MyButton();
            this.BT_Reset = new System.Windows.Forms.Button();
            this.BT_Add = new CustomControls.MyButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RB_Admin = new System.Windows.Forms.RadioButton();
            this.RB_User = new System.Windows.Forms.RadioButton();
            this.BT_AddClass = new CustomControls.BT_Add();
            this.BT_AddFaculty = new CustomControls.BT_Add();
            this.CBE_Classes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CBE_Faculties = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TE_Name = new DevExpress.XtraEditors.TextEdit();
            this.TE_StudentID = new DevExpress.XtraEditors.TextEdit();
            this.DE_DateOfBirth = new DevExpress.XtraEditors.DateEdit();
            this.PE_IMGStudent = new DevExpress.XtraEditors.PictureEdit();
            this.XOFD = new DevExpress.XtraEditors.XtraOpenFileDialog(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GC_Accounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Accounts)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Classes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_StudentID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PE_IMGStudent.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(800, 570);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // labelControl
            // 
            this.labelControl.AllowHtmlString = true;
            this.labelControl.Appearance.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl.Appearance.Options.UseFont = true;
            this.labelControl.Appearance.Options.UseForeColor = true;
            this.labelControl.Appearance.Options.UseTextOptions = true;
            this.labelControl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl.LineLocation = DevExpress.XtraEditors.LineLocation.Left;
            this.labelControl.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl.LineVisible = true;
            this.labelControl.Location = new System.Drawing.Point(0, 0);
            this.labelControl.Name = "labelControl";
            this.labelControl.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.labelControl.Size = new System.Drawing.Size(800, 30);
            this.labelControl.TabIndex = 1;
            this.labelControl.Text = "SIC Gang";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.GC_Accounts, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 246F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 570);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // GC_Accounts
            // 
            this.GC_Accounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC_Accounts.Location = new System.Drawing.Point(3, 249);
            this.GC_Accounts.MainView = this.GV_Accounts;
            this.GC_Accounts.Name = "GC_Accounts";
            this.GC_Accounts.Size = new System.Drawing.Size(794, 318);
            this.GC_Accounts.TabIndex = 0;
            this.GC_Accounts.TabStop = false;
            this.GC_Accounts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV_Accounts});
            // 
            // GV_Accounts
            // 
            this.GV_Accounts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.GV_Accounts.GridControl = this.GC_Accounts;
            this.GV_Accounts.Name = "GV_Accounts";
            this.GV_Accounts.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV_Accounts.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV_Accounts.OptionsBehavior.Editable = false;
            this.GV_Accounts.OptionsView.ShowGroupPanel = false;
            this.GV_Accounts.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.GV_Accounts_RowClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BT_Delete);
            this.panel1.Controls.Add(this.BT_Repair);
            this.panel1.Controls.Add(this.BT_Reset);
            this.panel1.Controls.Add(this.BT_Add);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.PE_IMGStudent);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 240);
            this.panel1.TabIndex = 1;
            // 
            // BT_Delete
            // 
            this.BT_Delete.Enabled = false;
            this.BT_Delete.Location = new System.Drawing.Point(551, 193);
            this.BT_Delete.Name = "BT_Delete";
            this.BT_Delete.Size = new System.Drawing.Size(95, 30);
            this.BT_Delete.TabIndex = 4;
            this.BT_Delete.TabStop = false;
            this.BT_Delete.Text = "Xóa";
            this.BT_Delete.UseVisualStyleBackColor = true;
            this.BT_Delete.Click += new System.EventHandler(this.BT_Delete_Click);
            // 
            // BT_Repair
            // 
            this.BT_Repair.Enabled = false;
            this.BT_Repair.Location = new System.Drawing.Point(427, 193);
            this.BT_Repair.Name = "BT_Repair";
            this.BT_Repair.Size = new System.Drawing.Size(95, 30);
            this.BT_Repair.TabIndex = 4;
            this.BT_Repair.TabStop = false;
            this.BT_Repair.Text = "Sửa";
            this.BT_Repair.UseVisualStyleBackColor = true;
            this.BT_Repair.Click += new System.EventHandler(this.BT_Repair_Click);
            // 
            // BT_Reset
            // 
            this.BT_Reset.BackgroundImage = global::SICGang.Properties.Resources.BT_Reset;
            this.BT_Reset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_Reset.FlatAppearance.BorderSize = 0;
            this.BT_Reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Reset.Location = new System.Drawing.Point(652, 198);
            this.BT_Reset.Name = "BT_Reset";
            this.BT_Reset.Size = new System.Drawing.Size(20, 20);
            this.BT_Reset.TabIndex = 5;
            this.BT_Reset.TabStop = false;
            this.BT_Reset.UseVisualStyleBackColor = true;
            this.BT_Reset.Click += new System.EventHandler(this.BT_Reset_Click);
            this.BT_Reset.MouseEnter += new System.EventHandler(this.BT_Reset_MouseEnter);
            this.BT_Reset.MouseLeave += new System.EventHandler(this.BT_Reset_MouseLeave);
            // 
            // BT_Add
            // 
            this.BT_Add.Location = new System.Drawing.Point(301, 193);
            this.BT_Add.Name = "BT_Add";
            this.BT_Add.Size = new System.Drawing.Size(95, 30);
            this.BT_Add.TabIndex = 4;
            this.BT_Add.TabStop = false;
            this.BT_Add.Text = "Thêm";
            this.BT_Add.UseVisualStyleBackColor = true;
            this.BT_Add.Click += new System.EventHandler(this.BT_Add_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::SICGang.Properties.Resources.FieldAdmin;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.RB_Admin);
            this.panel2.Controls.Add(this.RB_User);
            this.panel2.Controls.Add(this.BT_AddClass);
            this.panel2.Controls.Add(this.BT_AddFaculty);
            this.panel2.Controls.Add(this.CBE_Classes);
            this.panel2.Controls.Add(this.CBE_Faculties);
            this.panel2.Controls.Add(this.labelControl4);
            this.panel2.Controls.Add(this.labelControl5);
            this.panel2.Controls.Add(this.labelControl3);
            this.panel2.Controls.Add(this.labelControl2);
            this.panel2.Controls.Add(this.labelControl6);
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Controls.Add(this.TE_Name);
            this.panel2.Controls.Add(this.TE_StudentID);
            this.panel2.Controls.Add(this.DE_DateOfBirth);
            this.panel2.Location = new System.Drawing.Point(290, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(413, 150);
            this.panel2.TabIndex = 3;
            // 
            // RB_Admin
            // 
            this.RB_Admin.AutoSize = true;
            this.RB_Admin.Location = new System.Drawing.Point(194, 28);
            this.RB_Admin.Name = "RB_Admin";
            this.RB_Admin.Size = new System.Drawing.Size(89, 17);
            this.RB_Admin.TabIndex = 6;
            this.RB_Admin.Text = "Administrator";
            this.RB_Admin.TabStop = false;
            this.RB_Admin.UseVisualStyleBackColor = true;
            // 
            // RB_User
            // 
            this.RB_User.AutoSize = true;
            this.RB_User.Checked = true;
            this.RB_User.Location = new System.Drawing.Point(309, 28);
            this.RB_User.Name = "RB_User";
            this.RB_User.Size = new System.Drawing.Size(47, 17);
            this.RB_User.TabIndex = 6;
            this.RB_User.Text = "User";
            this.RB_User.TabStop = false;
            this.RB_User.UseVisualStyleBackColor = true;
            // 
            // BT_AddClass
            // 
            this.BT_AddClass.Location = new System.Drawing.Point(362, 121);
            this.BT_AddClass.Name = "BT_AddClass";
            this.BT_AddClass.Size = new System.Drawing.Size(20, 20);
            this.BT_AddClass.TabIndex = 5;
            this.BT_AddClass.TabStop = false;
            this.BT_AddClass.UseVisualStyleBackColor = true;
            this.BT_AddClass.Visible = false;
            this.BT_AddClass.Click += new System.EventHandler(this.BT_AddClass_Click);
            // 
            // BT_AddFaculty
            // 
            this.BT_AddFaculty.Location = new System.Drawing.Point(362, 74);
            this.BT_AddFaculty.Name = "BT_AddFaculty";
            this.BT_AddFaculty.Size = new System.Drawing.Size(20, 20);
            this.BT_AddFaculty.TabIndex = 5;
            this.BT_AddFaculty.TabStop = false;
            this.BT_AddFaculty.UseVisualStyleBackColor = true;
            this.BT_AddFaculty.Visible = false;
            this.BT_AddFaculty.Click += new System.EventHandler(this.BT_AddFaculty_Click);
            // 
            // CBE_Classes
            // 
            this.CBE_Classes.Location = new System.Drawing.Point(194, 123);
            this.CBE_Classes.Name = "CBE_Classes";
            this.CBE_Classes.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Classes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Classes.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Classes.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CBE_Classes.Properties.Sorted = true;
            this.CBE_Classes.Size = new System.Drawing.Size(162, 18);
            this.CBE_Classes.TabIndex = 4;
            this.CBE_Classes.TextChanged += new System.EventHandler(this.CBE_Classes_TextChanged);
            this.CBE_Classes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputSpace);
            // 
            // CBE_Faculties
            // 
            this.CBE_Faculties.Location = new System.Drawing.Point(194, 75);
            this.CBE_Faculties.Name = "CBE_Faculties";
            this.CBE_Faculties.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Faculties.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CBE_Faculties.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CBE_Faculties.Properties.Sorted = true;
            this.CBE_Faculties.Size = new System.Drawing.Size(162, 18);
            this.CBE_Faculties.TabIndex = 3;
            this.CBE_Faculties.SelectedIndexChanged += new System.EventHandler(this.CBE_Faculties_SelectedIndexChanged);
            this.CBE_Faculties.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputDigit);
            this.CBE_Faculties.Leave += new System.EventHandler(this.CBE_Faculties_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(194, 104);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(17, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Lớp";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(194, 57);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Khoa";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(47, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Ngày sinh";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Họ tên";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(194, 8);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(66, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "Loại tài khoản";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Mã SV";
            // 
            // TE_Name
            // 
            this.TE_Name.Location = new System.Drawing.Point(11, 75);
            this.TE_Name.Name = "TE_Name";
            this.TE_Name.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_Name.Size = new System.Drawing.Size(162, 18);
            this.TE_Name.TabIndex = 1;
            this.TE_Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputDigit);
            this.TE_Name.Leave += new System.EventHandler(this.TE_Name_Leave);
            // 
            // TE_StudentID
            // 
            this.TE_StudentID.Location = new System.Drawing.Point(11, 28);
            this.TE_StudentID.Name = "TE_StudentID";
            this.TE_StudentID.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.TE_StudentID.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.TE_StudentID.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_StudentID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_StudentID.Properties.MaxLength = 10;
            this.TE_StudentID.Size = new System.Drawing.Size(162, 18);
            this.TE_StudentID.TabIndex = 0;
            this.TE_StudentID.TextChanged += new System.EventHandler(this.TE_StudentID_TextChanged);
            this.TE_StudentID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NotAllowInputSpace);
            // 
            // DE_DateOfBirth
            // 
            this.DE_DateOfBirth.EditValue = null;
            this.DE_DateOfBirth.Location = new System.Drawing.Point(11, 123);
            this.DE_DateOfBirth.Name = "DE_DateOfBirth";
            this.DE_DateOfBirth.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_DateOfBirth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_DateOfBirth.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DE_DateOfBirth.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DE_DateOfBirth.Properties.ShowToday = false;
            this.DE_DateOfBirth.Size = new System.Drawing.Size(162, 18);
            this.DE_DateOfBirth.TabIndex = 2;
            // 
            // PE_IMGStudent
            // 
            this.PE_IMGStudent.Location = new System.Drawing.Point(120, 35);
            this.PE_IMGStudent.Name = "PE_IMGStudent";
            this.PE_IMGStudent.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.PE_IMGStudent.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.PE_IMGStudent.Size = new System.Drawing.Size(130, 150);
            this.PE_IMGStudent.TabIndex = 2;
            this.PE_IMGStudent.ImageChanged += new System.EventHandler(this.PE_IMGStudent_ImageChanged);
            this.PE_IMGStudent.Click += new System.EventHandler(this.PE_IMGStudent_Click);
            // 
            // XOFD
            // 
            this.XOFD.Filter = "JPeg Image|*.jpg|PNG Image|*.png|Bitmap Image|*.bmp";
            // 
            // SICGang
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.labelControl);
            this.Name = "SICGang";
            this.Size = new System.Drawing.Size(800, 600);
            this.Load += new System.EventHandler(this.SICGang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GC_Accounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Accounts)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Classes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBE_Faculties.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE_StudentID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DE_DateOfBirth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PE_IMGStudent.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraGrid.GridControl GC_Accounts;
        private DevExpress.XtraGrid.Views.Grid.GridView GV_Accounts;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private CustomControls.BT_Add BT_AddClass;
        private CustomControls.BT_Add BT_AddFaculty;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Classes;
        private DevExpress.XtraEditors.ComboBoxEdit CBE_Faculties;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit TE_Name;
        private DevExpress.XtraEditors.TextEdit TE_StudentID;
        private DevExpress.XtraEditors.DateEdit DE_DateOfBirth;
        private DevExpress.XtraEditors.PictureEdit PE_IMGStudent;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private CustomControls.MyButton BT_Add;
        private CustomControls.MyButton BT_Delete;
        private CustomControls.MyButton BT_Repair;
        private System.Windows.Forms.RadioButton RB_Admin;
        private System.Windows.Forms.RadioButton RB_User;
        private DevExpress.XtraEditors.XtraOpenFileDialog XOFD;

        #region Custom
        private Students StudentOld = null;
        private Accounts AccountOld = null;
        private Accounts UserAccount = null;
        public Accounts SetUserAccount { set => UserAccount = value; }
        public NotifyIcon Notify = null;
        public SICGang() => InitializeComponent();

        private bool IMGisOpen = false;
        private void PE_IMGStudent_Click(object sender, EventArgs e)
        {
            if (XOFD.ShowDialog() == DialogResult.OK)
            {
                PE_IMGStudent.Image = Image.FromFile(XOFD.FileName);
                IMGisOpen = true;
            }
        }

        #region Button Mouse Event
        private void BT_Reset_MouseEnter(object sender, EventArgs e) => BT_Reset.BackgroundImage = Properties.Resources.BT_Reset_Enter;

        private void BT_Reset_MouseLeave(object sender, EventArgs e) => BT_Reset.BackgroundImage = Properties.Resources.BT_Reset;
        #endregion

        private void NotAllowInputSpace(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
                e.Handled = true;
        }

        private void NotAllowInputDigit(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void ExportDataToField(Students _Student)
        {
            try
            {
                ExportDataToField(_Student.GetStudentID(), _Student.GetName(), _Student.GetDateOfBirthString(), _Student.GetFacultyName(), _Student.GetClassName());
            }
            catch { ExportDataToField(_Student.GetStudentID(), "", "", "", ""); }
        }

        private void ExportDataToField(string _StudentID, string _StudentName, string _DateOfBirth, string _FacultyName, string _ClassName)
        {
            TE_StudentID.Text = _StudentID;
            TE_Name.Text = _StudentName;
            DE_DateOfBirth.Text = _DateOfBirth.Split(' ')[0];
            CBE_Faculties.Text = _FacultyName;
            CBE_Classes.Text = _ClassName;
        }

        private void ExportDataToField(string _StudentID, string _StudentName, string _DateOfBirth, string _FacultyName, string _ClassName, Image _Img)
        {
            TE_StudentID.Text = _StudentID;
            TE_Name.Text = _StudentName;
            DE_DateOfBirth.Text = _DateOfBirth.Split(' ')[0];
            CBE_Faculties.Text = _FacultyName;
            CBE_Classes.Text = _ClassName;
            PE_IMGStudent.Image = _Img;
        }

        private Accounts GetAccountFromDataRow(DataRow dataRow)
        {
            var Account = new Accounts();

            Account.SetStudentID(Convert.ToString(dataRow.ItemArray[0]));
            Account.SetUser(Convert.ToString(dataRow.ItemArray[4]));
            Account.SetIsAdmin(Convert.ToBoolean(dataRow.ItemArray[5]));

            return Account;
        }

        private Students GetStudentFromField()
        {
            if (string.IsNullOrEmpty(TE_StudentID.Text) || string.IsNullOrEmpty(TE_Name.Text) || string.IsNullOrEmpty(DE_DateOfBirth.Text) || string.IsNullOrEmpty(CBE_Classes.Text)) return null;
            else
            {
                var Student = new Students();

                Student.SetStudentID(TE_StudentID.Text);
                Student.SetName(TE_Name.Text);
                Student.SetDateOfBirth(Convert.ToDateTime(DE_DateOfBirth.EditValue));
                Student.SetClassID(Classes.Get(CBE_Classes.Text).GetClassID());

                return Student;
            }
        }

        private void PE_IMGStudent_ImageChanged(object sender, EventArgs e)
        {
            if (PE_IMGStudent.Image != null)
            {
                PE_IMGStudent.Size = new Size()
                {
                    Height = PE_IMGStudent.Size.Height,
                    Width = (PE_IMGStudent.Image.Size.Width * PE_IMGStudent.Size.Height) / PE_IMGStudent.Image.Size.Height
                };
            }
            else PE_IMGStudent.Size = new Size() { Width = 130, Height = 150 };

            IMGisOpen = false;
        }

        private void TE_Name_Leave(object sender, EventArgs e)
        {
            TE_Name.Text = MyHelper.TitleFormat(TE_Name.Text);
        }

        private void GV_Accounts_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            TE_StudentID.Text = GV_Accounts.GetFocusedDataRow().ItemArray[0].ToString();
            if (Convert.ToBoolean(GV_Accounts.GetFocusedDataRow().ItemArray[5]))
                RB_Admin.Checked = true;
            else RB_User.Checked = true;

            TE_StudentID.Enabled = false;

            BT_Repair.Enabled = true;
            BT_Delete.Enabled = true;

            StudentOld = GetStudentFromField();
            AccountOld = GetAccountFromDataRow(GV_Accounts.GetFocusedDataRow());
        }
        #endregion

        private Button BT_Reset;
    }
}