﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.TB_User = new System.Windows.Forms.TextBox();
            this.TB_Pass = new System.Windows.Forms.TextBox();
            this.LB_LoginFalse = new System.Windows.Forms.Label();
            this.BT_Exit = new SICGang.GUI.CustomControls.BT_Exit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bT_Minimized1 = new SICGang.GUI.CustomControls.BT_Minimized();
            this.ErrProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.BT_Login = new SICGang.GUI.CustomControls.MyButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_User
            // 
            this.TB_User.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TB_User.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_User.Location = new System.Drawing.Point(147, 170);
            this.TB_User.Name = "TB_User";
            this.TB_User.Size = new System.Drawing.Size(279, 22);
            this.TB_User.TabIndex = 1;
            this.TB_User.TextChanged += new System.EventHandler(this.TB_User_TextChanged);
            this.TB_User.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyUp);
            // 
            // TB_Pass
            // 
            this.TB_Pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TB_Pass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_Pass.Location = new System.Drawing.Point(148, 225);
            this.TB_Pass.Name = "TB_Pass";
            this.TB_Pass.Size = new System.Drawing.Size(279, 19);
            this.TB_Pass.TabIndex = 2;
            this.TB_Pass.UseSystemPasswordChar = true;
            this.TB_Pass.TextChanged += new System.EventHandler(this.TB_Pass_TextChanged);
            this.TB_Pass.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyUp);
            // 
            // LB_LoginFalse
            // 
            this.LB_LoginFalse.AutoSize = true;
            this.LB_LoginFalse.BackColor = System.Drawing.Color.Transparent;
            this.LB_LoginFalse.ForeColor = System.Drawing.Color.Red;
            this.LB_LoginFalse.Location = new System.Drawing.Point(163, 204);
            this.LB_LoginFalse.Name = "LB_LoginFalse";
            this.LB_LoginFalse.Size = new System.Drawing.Size(234, 13);
            this.LB_LoginFalse.TabIndex = 15;
            this.LB_LoginFalse.Text = "Tên tài khoản hoặc mật khẩu không chính xác!";
            this.LB_LoginFalse.Visible = false;
            // 
            // BT_Exit
            // 
            this.BT_Exit.BackColor = System.Drawing.Color.Transparent;
            this.BT_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Exit.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Exit.ForeColor = System.Drawing.Color.White;
            this.BT_Exit.Location = new System.Drawing.Point(458, 0);
            this.BT_Exit.Name = "BT_Exit";
            this.BT_Exit.Size = new System.Drawing.Size(18, 18);
            this.BT_Exit.TabIndex = 16;
            this.BT_Exit.TabStop = false;
            this.BT_Exit.Text = "X";
            this.BT_Exit.UseVisualStyleBackColor = false;
            this.BT_Exit.Click += new System.EventHandler(this.BT_Exit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.bT_Minimized1);
            this.panel1.Controls.Add(this.BT_Exit);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 123);
            this.panel1.TabIndex = 17;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseUp);
            // 
            // bT_Minimized1
            // 
            this.bT_Minimized1.BackColor = System.Drawing.Color.Transparent;
            this.bT_Minimized1.FlatAppearance.BorderSize = 0;
            this.bT_Minimized1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bT_Minimized1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bT_Minimized1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bT_Minimized1.ForeColor = System.Drawing.Color.White;
            this.bT_Minimized1.Location = new System.Drawing.Point(436, -1);
            this.bT_Minimized1.Name = "bT_Minimized1";
            this.bT_Minimized1.Size = new System.Drawing.Size(20, 20);
            this.bT_Minimized1.TabIndex = 17;
            this.bT_Minimized1.TabStop = false;
            this.bT_Minimized1.Text = "−";
            this.bT_Minimized1.UseVisualStyleBackColor = false;
            // 
            // ErrProvider
            // 
            this.ErrProvider.ContainerControl = this;
            // 
            // BT_Login
            // 
            this.BT_Login.BackColor = System.Drawing.Color.Transparent;
            this.BT_Login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BT_Login.BackgroundImage")));
            this.BT_Login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_Login.CausesValidation = false;
            this.BT_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Login.ForeColor = System.Drawing.Color.White;
            this.BT_Login.Location = new System.Drawing.Point(148, 297);
            this.BT_Login.Name = "BT_Login";
            this.BT_Login.Size = new System.Drawing.Size(120, 40);
            this.BT_Login.TabIndex = 3;
            this.BT_Login.TabStop = false;
            this.BT_Login.Text = "Login";
            this.BT_Login.UseVisualStyleBackColor = false;
            this.BT_Login.Click += new System.EventHandler(this.ButtonLogin_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(500, 420);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LB_LoginFalse);
            this.Controls.Add(this.BT_Login);
            this.Controls.Add(this.TB_Pass);
            this.Controls.Add(this.TB_User);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.TransparencyKey = System.Drawing.Color.WhiteSmoke;
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TB_User;
        private System.Windows.Forms.TextBox TB_Pass;
        private CustomControls.MyButton BT_Login;
        private System.Windows.Forms.Label LB_LoginFalse;
        private CustomControls.BT_Exit BT_Exit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ErrorProvider ErrProvider;

        #region Custom form
        public Login() => InitializeComponent();

        private void ResetForm()
        {
            TB_User.Text = "";
            TB_Pass.Text = "";

            TB_User.Select();
        }

        #region TitleBar
        private void BT_Exit_Click(object sender, EventArgs e) => Application.Exit();

        #region Move
        bool isMouseDown;
        int xLast;
        int yLast;

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            xLast = e.X;
            yLast = e.Y;

            base.OnMouseDown(e);
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                int newY = this.Top + (e.Y - yLast);
                int newX = this.Left + (e.X - xLast);

                this.Location = new Point(newX, newY);
            }

            base.OnMouseMove(e);
        }

        private void TitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            base.OnMouseUp(e);
        }
        #endregion
        #endregion

        #region TextBox
        private void TB_Pass_TextChanged(object sender, EventArgs e)
        {
            LB_LoginFalse.Hide();

            ErrProvider.SetError(TB_Pass, null);
        }

        private void TB_User_TextChanged(object sender, EventArgs e)
        {
            LB_LoginFalse.Hide();

            ErrProvider.SetError(TB_User, null);
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (TB_Pass.Text != "" && TB_User.Text != "")
                    ButtonLogin_Click(sender, e);
                else if (TB_Pass.Text == "") TB_Pass.Select();
                else TB_User.Select();
        }
        #endregion

        #endregion

        private CustomControls.BT_Minimized bT_Minimized1;
    }
}