﻿using SICGang.Models;
using System;
using System.Windows.Forms;

namespace SICGang.GUI
{
    public partial class ChangeUser : Form
    {
        private Accounts UserAccount = null;

        private void BT_Ok_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TE_User.Text))
                if (BT_Ok.DialogResult != DialogResult.OK)
                {
                    LB_Error.Show();
                    TE_User.Focus();
                }
                else
                {
                    var ConfirmPass = new ConfirmPassword(UserAccount);
                    if (ConfirmPass.ShowDialog() == DialogResult.OK)
                    {
                        string NewUser = TE_User.Text;
                        string NewPassHashed = MyHelper.CreatePasswordHash(ConfirmPass.Password, NewUser);

                        if (SQL_Sever.ExecuteCommand($"exec proc_ChangeUsername '{UserAccount.GetUser()}', '{UserAccount.GetPassword()}', '{NewUser}', '{NewPassHashed}', {UserAccount.GetIsAdmin()}") != 0)
                        {
                            Notify.Visible = true;
                            Notify.ShowBalloonTip(3000, "Kết quả", "Đã đổi tên tài khoản thành công!", ToolTipIcon.Info);
                            UserAccount.SetUser(NewUser);
                            UserAccount.SetPassword(NewPassHashed);

                            Dispose();
                        }
                        else MessageBox.Show("Đổi tài khoản không thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
        }

        private void TE_User_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TE_User.Text))
                BT_Ok.DialogResult = Accounts.Exist(TE_User.Text) ? DialogResult.None : DialogResult.OK;
        }
    }
}