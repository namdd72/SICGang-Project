﻿using SICGang.Models;
using System;
using System.Windows.Forms;

namespace SICGang.GUI
{
    public partial class ConfirmPassword : Form
    {
        private Accounts UserAccount = null;
        public String Password = null;

        private void BT_Ok_Click(object sender, EventArgs e)
        {
            if (BT_Ok.DialogResult != DialogResult.OK)
            {
                LB_Error.Show();
                TE_Pass.Focus();
            }
            else Password = TE_Pass.Text;
        }

        private void TE_Pass_Leave(object sender, EventArgs e)
        {
            string PassHashed = MyHelper.CreatePasswordHash(TE_Pass.Text, UserAccount.GetUser());
            BT_Ok.DialogResult = (PassHashed == UserAccount.GetPassword()) ? DialogResult.OK : DialogResult.None;
        }
    }
}