﻿using SICGang.Models;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI
{
    partial class ConfirmPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmPassword));
            this.panel1 = new System.Windows.Forms.Panel();
            this.LB_Error = new System.Windows.Forms.Label();
            this.BT_Cancel = new System.Windows.Forms.Button();
            this.BT_Ok = new System.Windows.Forms.Button();
            this.TE_Pass = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Pass.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.LB_Error);
            this.panel1.Controls.Add(this.BT_Cancel);
            this.panel1.Controls.Add(this.BT_Ok);
            this.panel1.Controls.Add(this.TE_Pass);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(450, 155);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseUp);
            // 
            // LB_Error
            // 
            this.LB_Error.AutoSize = true;
            this.LB_Error.BackColor = System.Drawing.Color.White;
            this.LB_Error.ForeColor = System.Drawing.Color.Red;
            this.LB_Error.Location = new System.Drawing.Point(157, 93);
            this.LB_Error.Name = "LB_Error";
            this.LB_Error.Size = new System.Drawing.Size(139, 13);
            this.LB_Error.TabIndex = 2;
            this.LB_Error.Text = "Mật khẩu không chính xác!";
            this.LB_Error.Visible = false;
            // 
            // BT_Cancel
            // 
            this.BT_Cancel.BackColor = System.Drawing.Color.White;
            this.BT_Cancel.BackgroundImage = global::SICGang.Properties.Resources.BT_Cancel;
            this.BT_Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BT_Cancel.FlatAppearance.BorderSize = 0;
            this.BT_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Cancel.ForeColor = System.Drawing.Color.LimeGreen;
            this.BT_Cancel.Location = new System.Drawing.Point(328, 121);
            this.BT_Cancel.Name = "BT_Cancel";
            this.BT_Cancel.Size = new System.Drawing.Size(50, 17);
            this.BT_Cancel.TabIndex = 1;
            this.BT_Cancel.TabStop = false;
            this.BT_Cancel.Text = "\r\n";
            this.BT_Cancel.UseVisualStyleBackColor = false;
            this.BT_Cancel.Click += new System.EventHandler(this.BT_Cancel_Click);
            // 
            // BT_Ok
            // 
            this.BT_Ok.BackColor = System.Drawing.Color.White;
            this.BT_Ok.BackgroundImage = global::SICGang.Properties.Resources.BT_Ok;
            this.BT_Ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BT_Ok.FlatAppearance.BorderSize = 0;
            this.BT_Ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_Ok.ForeColor = System.Drawing.Color.LimeGreen;
            this.BT_Ok.Location = new System.Drawing.Point(397, 121);
            this.BT_Ok.Name = "BT_Ok";
            this.BT_Ok.Size = new System.Drawing.Size(25, 17);
            this.BT_Ok.TabIndex = 1;
            this.BT_Ok.TabStop = false;
            this.BT_Ok.UseVisualStyleBackColor = false;
            this.BT_Ok.Click += new System.EventHandler(this.BT_Ok_Click);
            // 
            // TE_Pass
            // 
            this.TE_Pass.Location = new System.Drawing.Point(29, 68);
            this.TE_Pass.Name = "TE_Pass";
            this.TE_Pass.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TE_Pass.Properties.UseSystemPasswordChar = true;
            this.TE_Pass.Size = new System.Drawing.Size(390, 18);
            this.TE_Pass.TabIndex = 0;
            this.TE_Pass.EditValueChanged += new System.EventHandler(this.TE_Pass_EditValueChanged);
            this.TE_Pass.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TE_Pass_KeyUp);
            this.TE_Pass.Leave += new System.EventHandler(this.TE_Pass_Leave);
            // 
            // ConfirmPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(450, 155);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfirmPassword";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfirmPassword";
            this.TransparencyKey = System.Drawing.Color.WhiteSmoke;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TE_Pass.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TE_Pass;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BT_Ok;
        private System.Windows.Forms.Button BT_Cancel;
        private System.Windows.Forms.Label LB_Error;

        #region Custom form
        public ConfirmPassword(Accounts _UserAccount)
        {
            InitializeComponent();
            UserAccount = _UserAccount;
        }

        #region Move
        bool isMouseDown;
        int xLast;
        int yLast;

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            xLast = e.X;
            yLast = e.Y;

            base.OnMouseDown(e);
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                int newY = this.Top + (e.Y - yLast);
                int newX = this.Left + (e.X - xLast);

                this.Location = new Point(newX, newY);
            }

            base.OnMouseMove(e);
        }

        private void TitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            base.OnMouseUp(e);
        }
        #endregion

        private void BT_Cancel_Click(object sender, EventArgs e) => Close();

        private void TE_Pass_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //TE_Pass_Leave(TE_Pass, null);
                BT_Ok.Focus();
                BT_Ok_Click(BT_Ok, null);
            }
        }

        private void TE_Pass_EditValueChanged(object sender, EventArgs e) => LB_Error.Hide();
        #endregion
    }
}