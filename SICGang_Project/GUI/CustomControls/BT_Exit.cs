﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI.CustomControls
{
    public partial class BT_Exit : Button
    {
        public BT_Exit() : base()
        {
            BackColor = Color.Transparent;
            FlatStyle = FlatStyle.Flat;
            FlatAppearance.BorderSize = 0;
            ForeColor = Color.White;
            Size = new Size(20, 20);
            FlatAppearance.MouseDownBackColor = Color.Transparent;
            FlatAppearance.MouseOverBackColor = Color.Transparent;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            ForeColor = Color.Red;
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            ForeColor = Color.White;
            base.OnMouseLeave(e);
        }

        protected override void OnClick(EventArgs e)
        {
            this.FindForm().Dispose();
            base.OnClick(e);
        }

        protected override void OnCreateControl()
        {
            Text = "X";
            base.OnCreateControl();
        }
    }
}
