﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI.CustomControls
{
    public partial class MyButton : Button
    {
        public MyButton():base()
        {
            BackgroundImage = Properties.Resources.BT_MouseLeave;
            BackgroundImageLayout = ImageLayout.Stretch;

            BackColor = Color.Transparent;
            FlatAppearance.BorderSize = 0;
            FlatStyle = FlatStyle.Flat;
            ForeColor = Color.White;
            FlatAppearance.MouseDownBackColor = Color.Transparent;
            FlatAppearance.MouseOverBackColor = Color.Transparent;
            CausesValidation = false;
            UseVisualStyleBackColor = true;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            BackgroundImage = Properties.Resources.BT_MouseEnter;
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            BackgroundImage = Properties.Resources.BT_MouseLeave;
            base.OnMouseLeave(e);
        }
    }
}