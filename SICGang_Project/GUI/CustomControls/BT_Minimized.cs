﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI.CustomControls
{
    public partial class BT_Minimized : Button
    {
        public BT_Minimized() : base()
        {
            BackColor = Color.Transparent;
            FlatAppearance.BorderSize = 0;
            FlatStyle = FlatStyle.Flat;
            ForeColor = Color.White;
            Size = new Size(20, 20);
            FlatAppearance.MouseDownBackColor = Color.Transparent;
            FlatAppearance.MouseOverBackColor = Color.Transparent;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            ForeColor = Color.Red;
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            ForeColor = Color.White;
            base.OnMouseLeave(e);
        }

        protected override void OnClick(EventArgs e)
        {
            this.Parent.FindForm().WindowState = FormWindowState.Minimized;
            base.OnClick(e);
        }

        protected override void OnCreateControl()
        {
            Text = "−";
            base.OnCreateControl();
        }
    }
}
