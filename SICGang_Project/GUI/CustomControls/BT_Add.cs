﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SICGang.GUI.CustomControls
{
    public partial class BT_Add : Button
    {
        public BT_Add():base()
        {
            BackgroundImage = Properties.Resources.BT_Add;
            BackgroundImageLayout = ImageLayout.Stretch;

            BackColor = Color.Transparent;
            FlatAppearance.BorderSize = 0;
            FlatStyle = FlatStyle.Flat;
            ForeColor = Color.White;
            FlatAppearance.MouseDownBackColor = Color.Transparent;
            FlatAppearance.MouseOverBackColor = Color.Transparent;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            BackgroundImage = Properties.Resources.BT_Add_MouseEnter;
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            BackgroundImage = Properties.Resources.BT_Add;
            base.OnMouseLeave(e);
        }
    }
}