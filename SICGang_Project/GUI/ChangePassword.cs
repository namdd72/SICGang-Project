﻿using System;
using System.Windows.Forms;
using SICGang.Models;
using SICGang.Properties;

namespace SICGang.GUI
{
    public partial class ChangePassword : Form
    {
        private Accounts UserAccount = null;

        private bool CheckCurentPass()
        {
            return (UserAccount.GetPassword() == MyHelper.CreatePasswordHash(TB_CurrentPass.Text, UserAccount.GetUser()));
        }

        private void BT_Change_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TB_CurrentPass.Text) || string.IsNullOrEmpty(TB_NewPass.Text) || string.IsNullOrEmpty(TB_ConfirmPass.Text))
            {
                if (string.IsNullOrEmpty(TB_CurrentPass.Text)) ErrProvider.SetError(TB_CurrentPass, "Mật khẩu cũ không được bỏ trống!");
                if (string.IsNullOrEmpty(TB_NewPass.Text)) ErrProvider.SetError(TB_NewPass, "Chưa nhập mật khẩu mới!");
                if (string.IsNullOrEmpty(TB_ConfirmPass.Text)) ErrProvider.SetError(TB_ConfirmPass, "Chưa xác nhận lại mật khẩu!");
            }
            else
            {
                if (TB_NewPass.Text == TB_ConfirmPass.Text)
                {
                    if (CheckCurentPass())
                    {
                        string NewPassHashed = MyHelper.CreatePasswordHash(TB_NewPass.Text, UserAccount.GetUser());
                        if (SQL_Sever.ExecuteCommand($"exec proc_ChangePassword '{UserAccount.GetUser()}', '{UserAccount.GetPassword()}', '{NewPassHashed}', {UserAccount.GetIsAdmin()}") != 0)
                        {
                            Notify.Visible = true;
                            Notify.ShowBalloonTip(3000, "Kết quả", "Đã đổi mật khẩu thành công!", ToolTipIcon.Info);
                            UserAccount.SetPassword(NewPassHashed);
                            this.Close();
                        }
                        else MessageBox.Show("Đổi mật khẩu không thành công!", "Kết quả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else LB_CheckCurrentPassFail.Show();
                }
                else LB_ErrConfirmPass.Show();
            }
        }
    }
}