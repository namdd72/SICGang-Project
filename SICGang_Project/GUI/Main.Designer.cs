﻿using DevExpress.XtraEditors;
using SICGang.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SICGang.GUI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TitleBar = new System.Windows.Forms.Panel();
            this.LB_Logout = new System.Windows.Forms.LinkLabel();
            this.BT_About = new System.Windows.Forms.Button();
            this.BT_Profile = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.BT_Max = new System.Windows.Forms.Button();
            this.BT_Mini = new System.Windows.Forms.Button();
            this.BT_Exit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.EFUC = new DevExpress.XtraGrid.Views.Grid.EditFormUserControl();
            this.TC_Menu = new DevExpress.XtraToolbox.ToolboxControl();
            this.TG_SICGang = new DevExpress.XtraToolbox.ToolboxGroup();
            this.TG_Home = new DevExpress.XtraToolbox.ToolboxGroup();
            this.TG_View = new DevExpress.XtraToolbox.ToolboxGroup();
            this.toolboxItem1 = new DevExpress.XtraToolbox.ToolboxItem();
            this.toolboxItem2 = new DevExpress.XtraToolbox.ToolboxItem();
            this.toolboxItem3 = new DevExpress.XtraToolbox.ToolboxItem();
            this.toolboxItem4 = new DevExpress.XtraToolbox.ToolboxItem();
            this.Notify = new System.Windows.Forms.NotifyIcon(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.TitleBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.EFUC.SetBoundPropertyName(this.tableLayoutPanel1, "");
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.TitleBar, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(837, 525);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // TitleBar
            // 
            this.EFUC.SetBoundPropertyName(this.TitleBar, "");
            this.TitleBar.Controls.Add(this.LB_Logout);
            this.TitleBar.Controls.Add(this.BT_About);
            this.TitleBar.Controls.Add(this.BT_Profile);
            this.TitleBar.Controls.Add(this.label4);
            this.TitleBar.Controls.Add(this.BT_Max);
            this.TitleBar.Controls.Add(this.BT_Mini);
            this.TitleBar.Controls.Add(this.BT_Exit);
            this.TitleBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TitleBar.Location = new System.Drawing.Point(3, 3);
            this.TitleBar.Name = "TitleBar";
            this.TitleBar.Size = new System.Drawing.Size(831, 17);
            this.TitleBar.TabIndex = 2;
            this.TitleBar.DoubleClick += new System.EventHandler(this.BT_Max_Click);
            this.TitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            this.TitleBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseMove);
            this.TitleBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseUp);
            // 
            // LB_Logout
            // 
            this.LB_Logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LB_Logout.AutoSize = true;
            this.EFUC.SetBoundPropertyName(this.LB_Logout, "");
            this.LB_Logout.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LB_Logout.Location = new System.Drawing.Point(743, 1);
            this.LB_Logout.Name = "LB_Logout";
            this.LB_Logout.Size = new System.Drawing.Size(43, 13);
            this.LB_Logout.TabIndex = 1;
            this.LB_Logout.Text = "Log out";
            this.LB_Logout.TabStop = false;
            this.LB_Logout.Click += new System.EventHandler(this.LB_Logout_Click);
            // 
            // BT_About
            // 
            this.BT_About.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BT_About.BackgroundImage = global::SICGang.Properties.Resources.icons8_info_480;
            this.BT_About.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EFUC.SetBoundPropertyName(this.BT_About, "");
            this.BT_About.FlatAppearance.BorderSize = 0;
            this.BT_About.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_About.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BT_About.Location = new System.Drawing.Point(813, 1);
            this.BT_About.Name = "BT_About";
            this.BT_About.Size = new System.Drawing.Size(14, 14);
            this.BT_About.TabIndex = 7;
            this.BT_About.TabStop = false;
            this.BT_About.UseVisualStyleBackColor = true;
            this.BT_About.Click += new System.EventHandler(this.BT_About_Click);
            // 
            // BT_Profile
            // 
            this.BT_Profile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BT_Profile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BT_Profile.BackgroundImage")));
            this.BT_Profile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EFUC.SetBoundPropertyName(this.BT_Profile, "");
            this.BT_Profile.FlatAppearance.BorderSize = 0;
            this.BT_Profile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Profile.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BT_Profile.Location = new System.Drawing.Point(792, 0);
            this.BT_Profile.Name = "BT_Profile";
            this.BT_Profile.Size = new System.Drawing.Size(15, 15);
            this.BT_Profile.TabIndex = 7;
            this.BT_Profile.TabStop = false;
            this.BT_Profile.UseVisualStyleBackColor = true;
            this.BT_Profile.Click += new System.EventHandler(this.BT_Profile_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.EFUC.SetBoundPropertyName(this.label4, "");
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(71, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Multimedia Room Management";
            this.label4.DoubleClick += new System.EventHandler(this.BT_Max_Click);
            this.label4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            this.label4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseMove);
            this.label4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseUp);
            // 
            // BT_Max
            // 
            this.BT_Max.BackgroundImage = global::SICGang.Properties.Resources.BT_Maximum_MouseLeave;
            this.BT_Max.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EFUC.SetBoundPropertyName(this.BT_Max, "");
            this.BT_Max.FlatAppearance.BorderSize = 0;
            this.BT_Max.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Max.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BT_Max.Location = new System.Drawing.Point(50, 1);
            this.BT_Max.Name = "BT_Max";
            this.BT_Max.Size = new System.Drawing.Size(15, 15);
            this.BT_Max.TabIndex = 5;
            this.BT_Max.TabStop = false;
            this.BT_Max.UseVisualStyleBackColor = true;
            this.BT_Max.Click += new System.EventHandler(this.BT_Max_Click);
            this.BT_Max.MouseEnter += new System.EventHandler(this.BT_Max_MouseEnter);
            this.BT_Max.MouseLeave += new System.EventHandler(this.BT_Max_MouseLeave);
            // 
            // BT_Mini
            // 
            this.BT_Mini.BackgroundImage = global::SICGang.Properties.Resources.BT_Minimum_MouseLeave;
            this.BT_Mini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EFUC.SetBoundPropertyName(this.BT_Mini, "");
            this.BT_Mini.FlatAppearance.BorderSize = 0;
            this.BT_Mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Mini.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BT_Mini.Location = new System.Drawing.Point(26, 1);
            this.BT_Mini.Name = "BT_Mini";
            this.BT_Mini.Size = new System.Drawing.Size(15, 15);
            this.BT_Mini.TabIndex = 4;
            this.BT_Mini.TabStop = false;
            this.BT_Mini.UseVisualStyleBackColor = true;
            this.BT_Mini.Click += new System.EventHandler(this.BT_Mini_Click);
            this.BT_Mini.MouseEnter += new System.EventHandler(this.BT_Mini_MouseEnter);
            this.BT_Mini.MouseLeave += new System.EventHandler(this.BT_Mini_MouseLeave);
            // 
            // BT_Exit
            // 
            this.BT_Exit.BackgroundImage = global::SICGang.Properties.Resources.BT_Exit_MouseLeave;
            this.BT_Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EFUC.SetBoundPropertyName(this.BT_Exit, "");
            this.BT_Exit.FlatAppearance.BorderSize = 0;
            this.BT_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BT_Exit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BT_Exit.Location = new System.Drawing.Point(2, 1);
            this.BT_Exit.Name = "BT_Exit";
            this.BT_Exit.Size = new System.Drawing.Size(15, 15);
            this.BT_Exit.TabIndex = 3;
            this.BT_Exit.TabStop = false;
            this.BT_Exit.UseVisualStyleBackColor = true;
            this.BT_Exit.Click += new System.EventHandler(this.BT_Exit_Click);
            this.BT_Exit.MouseEnter += new System.EventHandler(this.BT_Exit_MouseEnter);
            this.BT_Exit.MouseLeave += new System.EventHandler(this.BT_Exit_MouseLeave);
            // 
            // panel1
            // 
            this.EFUC.SetBoundPropertyName(this.panel1, "");
            this.panel1.Controls.Add(this.EFUC);
            this.panel1.Controls.Add(this.TC_Menu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 23);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(837, 502);
            this.panel1.TabIndex = 3;
            // 
            // EFUC
            // 
            this.EFUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EFUC.Location = new System.Drawing.Point(26, 0);
            this.EFUC.Name = "EFUC";
            this.EFUC.Size = new System.Drawing.Size(811, 502);
            this.EFUC.TabIndex = 7;
            // 
            // TC_Menu
            // 
            this.TC_Menu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TC_Menu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.EFUC.SetBoundPropertyName(this.TC_Menu, "");
            this.TC_Menu.Caption = "Menu";
            this.TC_Menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.TC_Menu.Groups.Add(this.TG_SICGang);
            this.TC_Menu.Groups.Add(this.TG_Home);
            this.TC_Menu.Groups.Add(this.TG_View);
            this.TC_Menu.Location = new System.Drawing.Point(0, 0);
            this.TC_Menu.MaximumSize = new System.Drawing.Size(150, 0);
            this.TC_Menu.Name = "TC_Menu";
            this.TC_Menu.OptionsMinimizing.State = DevExpress.XtraToolbox.ToolboxState.Minimized;
            this.TC_Menu.OptionsView.MenuButtonCaption = "";
            this.TC_Menu.OptionsView.ShowMenuButton = false;
            this.TC_Menu.OptionsView.ShowToolboxCaption = true;
            this.TC_Menu.Size = new System.Drawing.Size(26, 502);
            this.TC_Menu.TabIndex = 0;
            this.TC_Menu.TabStop = false;
            this.TC_Menu.Text = "Menu";
            this.TC_Menu.SelectedGroupChanged += new DevExpress.XtraToolbox.ToolboxSelectedGroupChangedEventHandler(this.TC_Menu_SelectedGroupChanged);
            // 
            // TG_SICGang
            // 
            this.TG_SICGang.Caption = "SIC Gang";
            this.TG_SICGang.ImageOptions.Image = global::SICGang.Properties.Resources.Logo_SICGang;
            this.TG_SICGang.Name = "TG_SICGang";
            // 
            // TG_Home
            // 
            this.TG_Home.Caption = "Home";
            this.TG_Home.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("TG_Home.ImageOptions.Image")));
            this.TG_Home.Name = "TG_Home";
            // 
            // TG_View
            // 
            this.TG_View.Caption = "View";
            this.TG_View.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("TG_View.ImageOptions.SvgImage")));
            this.TG_View.Name = "TG_View";
            // 
            // toolboxItem1
            // 
            this.toolboxItem1.Caption = "toolboxItem1";
            this.toolboxItem1.Name = "toolboxItem1";
            // 
            // toolboxItem2
            // 
            this.toolboxItem2.Caption = "toolboxItem2";
            this.toolboxItem2.Name = "toolboxItem2";
            // 
            // toolboxItem3
            // 
            this.toolboxItem3.Caption = "toolboxItem3";
            this.toolboxItem3.Name = "toolboxItem3";
            // 
            // toolboxItem4
            // 
            this.toolboxItem4.Caption = "toolboxItem4";
            this.toolboxItem4.Name = "toolboxItem4";
            // 
            // Notify
            // 
            this.Notify.Icon = ((System.Drawing.Icon)(resources.GetObject("Notify.Icon")));
            this.Notify.Text = "SIC GANG";
            this.Notify.Visible = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.EFUC.SetBoundPropertyName(this, "");
            this.ClientSize = new System.Drawing.Size(837, 525);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Multimedia Room Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.TitleBar.ResumeLayout(false);
            this.TitleBar.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel TitleBar;
        private System.Windows.Forms.Button BT_Profile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BT_Max;
        private System.Windows.Forms.Button BT_Mini;
        private System.Windows.Forms.Button BT_Exit;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraToolbox.ToolboxControl TC_Menu;
        private DevExpress.XtraToolbox.ToolboxGroup TG_SICGang;
        private DevExpress.XtraToolbox.ToolboxItem toolboxItem1;
        private DevExpress.XtraToolbox.ToolboxItem toolboxItem2;
        private DevExpress.XtraToolbox.ToolboxItem toolboxItem3;
        private DevExpress.XtraToolbox.ToolboxItem toolboxItem4;
        private DevExpress.XtraToolbox.ToolboxGroup TG_Home;
        private System.Windows.Forms.LinkLabel LB_Logout;
        private DevExpress.XtraToolbox.ToolboxGroup TG_View;
        private DevExpress.XtraGrid.Views.Grid.EditFormUserControl EFUC;

        #region Custom form

        public Main(Accounts _UserAcount)
        {
            InitializeComponent();

            UserAccount = _UserAcount;
            (ListUserControls[0] as UserControls.SICGang).SetUserAccount = UserAccount;
            (ListUserControls[1] as UserControls.Home).SetUserAccount = UserAccount;
            //TC_Menu.Groups[0].Visible = UserAccount.IsAdmin;
            TC_Menu.Visible = UserAccount.GetIsAdmin();
            TC_Menu.SelectedGroupIndex = 1;
        }

        private static Accounts UserAccount = null;
        private List<XtraUserControl> ListUserControls = new List<XtraUserControl>()
        {
            new UserControls.SICGang(){ Dock = DockStyle.Fill },
            new UserControls.Home(){ Dock = DockStyle.Fill },
            new UserControls.View(){ Dock = DockStyle.Fill }
        };

        private void TC_Menu_SelectedGroupChanged(object sender, DevExpress.XtraToolbox.ToolboxSelectedGroupChangedEventArgs e)
        {
            EFUC.Controls.Clear();
            EFUC.Controls.Add(ListUserControls[TC_Menu.SelectedGroupIndex]);
            TC_Menu.OptionsMinimizing.State = DevExpress.XtraToolbox.ToolboxState.Minimized;
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Notify.Visible = false;
        }

        #region TitleBar
        private void BT_Exit_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }

        private void BT_Mini_Click(object sender, EventArgs e) => WindowState = FormWindowState.Minimized;

        bool Maximized = false;
        private void BT_Max_Click(object sender, EventArgs e)
        {
            if (!Maximized)
            {
                this.Location = new Point(Screen.GetWorkingArea(this).X, Screen.GetWorkingArea(this).Y);
                this.Size = new Size(Screen.GetWorkingArea(this).Width, Screen.GetWorkingArea(this).Height);
            }
            else this.Size = new Size(837, 525);
            //this.Size = Maximized ? new Size(837, 525) : new Size(Screen.GetWorkingArea(this).Width, Screen.GetWorkingArea(this).Height);
            Maximized = !Maximized;
        }

        #region Move
        bool isMouseDown;
        int xLast;
        int yLast;

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            xLast = e.X;
            yLast = e.Y;

            base.OnMouseDown(e);
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                if (Maximized) BT_Max_Click(null, null);

                int newY = this.Top + (e.Y - yLast);
                int newX = this.Left + (e.X - xLast);

                this.Location = new Point(newX, newY);
            }

            base.OnMouseMove(e);
        }

        private void TitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            base.OnMouseUp(e);
        }
        #endregion
        #endregion

        #region Button, LinkLable
        private void BT_Exit_MouseEnter(object sender, EventArgs e) => BT_Exit.BackgroundImage = Properties.Resources.BT_Exit_MouseEnter;

        private void BT_Exit_MouseLeave(object sender, EventArgs e) => BT_Exit.BackgroundImage = Properties.Resources.BT_Exit_MouseLeave;

        private void BT_Mini_MouseEnter(object sender, EventArgs e) => BT_Mini.BackgroundImage = Properties.Resources.BT_Minimum_MouseEnter;

        private void BT_Mini_MouseLeave(object sender, EventArgs e) => BT_Mini.BackgroundImage = Properties.Resources.BT_Minimum_MouseLeave;

        private void BT_Max_MouseEnter(object sender, EventArgs e) => BT_Max.BackgroundImage = Properties.Resources.BT_Maximum_MouseEnter;

        private void BT_Max_MouseLeave(object sender, EventArgs e) => BT_Max.BackgroundImage = Properties.Resources.BT_Maximum_MouseLeave;

        private void LB_Logout_Click(object sender, EventArgs e) => Close();

        private void BT_Profile_Click(object sender, EventArgs e) => new Profile(UserAccount).ShowDialog();

        private void BT_About_Click(object sender, System.EventArgs e) => new AboutBox().ShowDialog();
        #endregion

        #endregion

        public NotifyIcon Notify;
        private Button BT_About;
    }
}