﻿using System;

namespace SICGang.Models
{
    public class AccessHistories
    {
        private string StudentID;
        private DateTime TimeEnter;
        private DateTime TimeLeave;

        public AccessHistories() { }

        public AccessHistories(string _StudentID, DateTime _TimeEnter, DateTime _TimeLeave)
        {
            SetStudentID(_StudentID);
            SetTimeEnter(_TimeEnter);
            SetTimeLeave(_TimeLeave);
        }

        public string GetStudentID()
        {
            return StudentID;
        }

        public void SetStudentID(string value)
        {
            StudentID = value;
        }

        public DateTime GetTimeEnter()
        {
            return TimeEnter;
        }

        public void SetTimeEnter(DateTime value)
        {
            TimeEnter = value;
        }

        public DateTime GetTimeLeave()
        {
            return TimeLeave;
        }

        public void SetTimeLeave(DateTime value)
        {
            TimeLeave = value;
        }
        
        /// <summary>
        /// Insert AccessHistory to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_AccessHistories(StudentID, TimeEnter) values('{GetStudentID()}', '{GetTimeEnter()}')";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Update TimeLeave in Database and return result update.
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string Query = $"update tb_AccessHistories set TimeLeave = '{GetTimeLeave()}' where StudentID = '{GetStudentID()}' and TimeLeave is null";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Delete AccessHistory in Database and return result delete.
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            string Query = $"delete tb_AccessHistories where StudentID = '{GetStudentID()}' and TimeLeave is null";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        public static bool CkeckStudentInRoom(string _StudentID)
        {
            string Query = $"select dbo.func_CheckStudentInRoom('{_StudentID}')";

            return Convert.ToBoolean(SQL_Sever.ExecuteScalar(Query));
        }
    }
}