﻿using System;
using System.Text;
using System.Web.Security;

namespace SICGang.Models
{
    public static class MyHelper
    {
        public static string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, Convert.ToBase64String(Encoding.ASCII.GetBytes(salt)));
            return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha1");
        }

        public static string CreatePasswordHash(Accounts accounts) => CreatePasswordHash(accounts.GetPassword(), accounts.GetUser());

        public static string TitleFormat(string _Text)
        {
            string Result = "";

            foreach (var i in _Text.Split(' '))
                if (i != "") Result += i[0].ToString().ToUpper() + i.Substring(1).ToLower() + " ";

            if (Result != "") Result = Result.Substring(0, Result.Length - 1);

            return Result;
        }
    }
}