﻿using System;
using System.Text;

namespace SICGang.Models
{
    class Scan
    {
        private DateTime Time;
        private StringBuilder Text = new StringBuilder("", 10);

        public Scan() { Time = DateTime.Now; }

        public Scan(string text)
        {
            Time = DateTime.Now;
            Text.Append(text);
        }

        public string GetText()
        {
            return Text.ToString();
        }

        public void AddText(char tmp)
        {
            if (Time.AddSeconds(1) <= DateTime.Now)
            {
                Time = DateTime.Now;
                Text.Clear();
            }

            Text.Append(tmp);
        }
    }
}