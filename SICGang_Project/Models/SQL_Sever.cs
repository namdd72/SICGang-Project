﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SICGang.Models
{
    class SQL_Sever
    {
        private static string ConnStr = @"Data Source=10.0.3.37,1433;Initial Catalog=SICGANG_Project;User ID=SA;Password=sicgang@123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //private static string ConnStr = @"Data Source=DESKTOP-LISTS81;Initial Catalog=SICGANG_Project;Integrated Security=True;";
        private static SqlConnection Conn = new SqlConnection(ConnStr);
        private static SqlCommand Cmd = new SqlCommand();

        public static bool TestConnect(string ConnStr)
        {
            SqlConnection Conn = new SqlConnection(ConnStr);

            try
            {
                Conn.Open();
                return true;
            }
            catch { return false; }
        }

        public static bool SetDataSource(string DataSource)
        {
            ConnStr = $"Data Source={DataSource};Initial Catalog=SICGANG_Project;User ID=SA;Password=sicgang@123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            if (TestConnect(ConnStr))
            {
                Conn = new SqlConnection(ConnStr);
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Opens Database Connection.
        /// </summary>
        public static void Connect()
        {
            if (Conn.State == ConnectionState.Closed)
                Conn.Open();
        }

        /// <summary>
        /// Initializes a new instance of the SqlDataAdapter class with Query.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static SqlDataAdapter DataAdapter(string Query)
        {
            Connect();

            var Result = new SqlDataAdapter(Query, Conn);

            Disconnect();

            return Result;
        }

        /// <summary>
        /// Execute Query and returns result by DataTable.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static DataTable ExecuteDataTable(string Query)
        {
            var Result = new DataTable();

            DataAdapter(Query).Fill(Result);

            return Result;
        }

        /// <summary>
        /// Execute Query and returns a column in DataTable as List object.
        /// </summary>
        /// <param name="Query"></param>
        /// <param name="Column"></param>
        /// <returns></returns>
        public static List<object> ExecuteList(string Query, int Column = 0)
        {
            return ExecuteDataTable(Query).AsEnumerable().Select(Row => Row[Column]).ToList();
        }

        /// <summary>
        /// Execute Query and returns first row in result.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static DataRow ReadRow(string Query)
        {
            return ExecuteDataTable(Query).Rows[0];
        }

        /// <summary>
        /// Executes Query and returns first column of first row in result.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static object ExecuteScalar(string Query)
        {
            Connect();

            Cmd.Connection = Conn;
            Cmd.CommandText = Query;
            var Result = Cmd.ExecuteScalar();

            Disconnect();

            return Result;
        }

        /// <summary>
        /// Execute Query and returns number of rows affected.
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static int ExecuteCommand(string Query)
        {
            Connect();

            Cmd.Connection = Conn;
            Cmd.CommandText = Query;
            int RowsAffected = Cmd.ExecuteNonQuery();

            Disconnect();

            return RowsAffected;
        }

        /// <summary>
        /// Closes Connection to Database.
        /// </summary>
        public static void Disconnect()
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
        }
    }
}