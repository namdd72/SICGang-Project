﻿namespace CreatePasswordHash
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_User = new System.Windows.Forms.TextBox();
            this.TB_Pass = new System.Windows.Forms.TextBox();
            this.RTB_PasswordHashed = new System.Windows.Forms.RichTextBox();
            this.BT_Create = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TB_User
            // 
            this.TB_User.Location = new System.Drawing.Point(84, 36);
            this.TB_User.Name = "TB_User";
            this.TB_User.Size = new System.Drawing.Size(243, 20);
            this.TB_User.TabIndex = 0;
            // 
            // TB_Pass
            // 
            this.TB_Pass.Location = new System.Drawing.Point(84, 73);
            this.TB_Pass.Name = "TB_Pass";
            this.TB_Pass.Size = new System.Drawing.Size(243, 20);
            this.TB_Pass.TabIndex = 1;
            // 
            // RTB_PasswordHashed
            // 
            this.RTB_PasswordHashed.Location = new System.Drawing.Point(84, 128);
            this.RTB_PasswordHashed.Name = "RTB_PasswordHashed";
            this.RTB_PasswordHashed.Size = new System.Drawing.Size(243, 96);
            this.RTB_PasswordHashed.TabIndex = 3;
            this.RTB_PasswordHashed.Text = "";
            // 
            // BT_Create
            // 
            this.BT_Create.Location = new System.Drawing.Point(252, 99);
            this.BT_Create.Name = "BT_Create";
            this.BT_Create.Size = new System.Drawing.Size(75, 23);
            this.BT_Create.TabIndex = 2;
            this.BT_Create.Text = " Create";
            this.BT_Create.UseVisualStyleBackColor = true;
            this.BT_Create.Click += new System.EventHandler(this.BT_Create_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = " Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = " Password";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 236);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BT_Create);
            this.Controls.Add(this.RTB_PasswordHashed);
            this.Controls.Add(this.TB_Pass);
            this.Controls.Add(this.TB_User);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_User;
        private System.Windows.Forms.TextBox TB_Pass;
        private System.Windows.Forms.RichTextBox RTB_PasswordHashed;
        private System.Windows.Forms.Button BT_Create;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

