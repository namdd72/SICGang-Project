﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using System.Windows.Forms;

namespace CreatePasswordHash
{
    public partial class Form1 : Form
    {
        public Form1() => InitializeComponent();

        private string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, Convert.ToBase64String(Encoding.ASCII.GetBytes(salt)));
            return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha1");
        }

        private void BT_Create_Click(object sender, EventArgs e)
        {
            RTB_PasswordHashed.Text = "";
            RTB_PasswordHashed.Text = CreatePasswordHash(TB_Pass.Text, TB_User.Text);
        }
    }
}
