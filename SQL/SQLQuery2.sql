﻿-- Kiểm tra Username, Password nếu đúng trả về quyền của user, ngược lại trả về -1
create function func_Login(@User varchar(50), @Password varchar(100))
returns int
as
begin
	declare @Check int = (select isAdmin from tb_Accounts where [User] = @User and [Password] = @Password);

	if @Check is null
		return -1
		
	return @Check
end

go

-- Lấy thông tin của Account
create function func_GetAccount(@User varchar(50), @Pass varchar(100))
returns table
as
return (select * from tb_Accounts where [User] = @User and [Password] = @Pass);

go

-- Đổi Username và Password được băm lại
create proc proc_ChangeUsername(@User varchar(50), @Pass varchar(100), @NewUser varchar(50), @NewPass varchar(100), @isAdmin bit)
as update tb_Accounts set [User] = @NewUser, [Password] = @NewPass where [User] = @User and [Password] = @Pass and isAdmin = @isAdmin

go

-- Đổi Password
create proc proc_ChangePassword(@User varchar(50), @Pass varchar(100), @NewPass varchar(100), @isAdmin bit)
as update tb_Accounts set [Password] = @NewPass where [User] = @User and [Password] = @Pass and isAdmin = @isAdmin

go

-- Lấy Ds SV đang trong phòng
create view view_PeopleInRoomNow as
select tb_Students.StudentID as N'Mã SV', tb_Students.[Name] as N'Họ tên', DateOfBirth as N'Ngày sinh', tb_Classes.[Name] as N'Lớp', tb_Faculties.[Name] as N'Khoa'
from tb_AccessHistories, tb_Students, tb_Classes, tb_Faculties
where tb_AccessHistories.StudentID = tb_Students.StudentID
and tb_Classes.ClassID = tb_Students.ClassID
and tb_Classes.FacultyID = tb_Faculties.FacultyID
--and TimeEnter >= CAST(GETDATE() as date) 
and TimeLeave is null;

go

-- Lấy ds thông tin các account
create view view_Accounts as
select tb_Students.StudentID as N'Mã SV', tb_Students.[Name] as N'Họ tên', DateOfBirth as N'Ngày sinh', tb_Classes.[Name] as 'Lớp', [User] as N'Tên tài khoản', isAdmin as N'Quản trị'
from tb_Accounts, tb_Students, tb_Classes 
where tb_Accounts.StudentID = tb_Students.StudentID 
and tb_Students.ClassID = tb_Classes.ClassID

go

-- Lấy Ds Lớp thuộc Khoa bằng tên khoa
create function func_GetClassesFromFacultyName(@Name nvarchar(100))
returns table
as
return select tb_Classes.[Name] from tb_Classes, tb_Faculties 
where tb_Faculties.FacultyID = tb_Classes.FacultyID
and tb_Faculties.[Name] = @Name;

go

-- Lấy tên Khoa của Lớp bằng tên lớp
create function func_GetFacultyNameByClassName(@ClassName varchar(10))
returns nvarchar(100)
as
begin
return (select tb_Faculties.[Name] from tb_Faculties, tb_Classes 
where tb_Classes.FacultyID = tb_Faculties.FacultyID
and tb_Classes.[Name] = @ClassName);
end

go

-- Không cho phép thêm lịch sử ra vào khi sv chưa ra khỏi phòng
create trigger Trg_InsertAccessHistory on tb_AccessHistories for insert as
begin
if (select count(*) from tb_AccessHistories where StudentID = (select StudentID from inserted) and TimeLeave is null) > 1
rollback tran
end

go

-- Lấy Datalogs từ ngày đến ngày, loại tìa khoản tác động
create function func_GetDatalogs(@StartDate date, @EndDate date)
returns table
return select tb_Datalogs.StudentID as N'Mã sinh viên', [Name] as N'Người dùng', [Time] as N'Thời gian', ModifiedContent as N'Nội dung tác động'
from tb_Datalogs, tb_Accounts, tb_Students
where tb_Datalogs.StudentID = tb_Students.StudentID
and ([Time] between @StartDate and dateadd(day,1,@EndDate))
group by tb_Datalogs.StudentID, [Name], [Time], ModifiedContent;

go

-- Lấy lịch sử ra vào từ ngày đến ngày
create function func_GetAccessHistories(@StartDate date, @EndDate date)
returns table
return select tb_Students.StudentID as N'Mã SV', tb_Students.[Name] as N'Họ tên', DateOfBirth as N'Ngày sinh', tb_Classes.[Name] as N'Lớp', tb_Faculties.[Name] as N'Khoa', count(tb_Students.StudentID) as N'Số lần vào phòng'
from tb_AccessHistories, tb_Students, tb_Classes, tb_Faculties
where tb_AccessHistories.StudentID = tb_Students.StudentID
and tb_Classes.ClassID = tb_Students.ClassID
and tb_Classes.FacultyID = tb_Faculties.FacultyID
and (TimeEnter between @StartDate and  dateadd(day,1,@EndDate))
group by tb_Students.StudentID, tb_Students.[Name], DateOfBirth, tb_Classes.[Name], tb_Faculties.[Name]

go

-- Kiểm tra xem SV có còn trong phòng không
create function func_CheckStudentInRoom(@StudentID char(10))
returns bit
as
begin
if exists(select StudentID from tb_AccessHistories where StudentID = @StudentID and TimeLeave is null) return 1
return 0
end

go