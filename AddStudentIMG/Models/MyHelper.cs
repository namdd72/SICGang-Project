﻿using System;
using System.Text;
using System.Web.Security;

namespace SICGang.Models
{
    public static class MyHelper
    {
        public static string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, Convert.ToBase64String(Encoding.ASCII.GetBytes(salt)));
            return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha1");
        }

        public static string CreatePasswordHash(Accounts accounts) => CreatePasswordHash(accounts.GetPassword(), accounts.GetUser());
    }
}