﻿using System;
using System.Data;

namespace SICGang.Models
{
    public class Classes
    {
        private int ClassID;
        private string Name;
        private int FacultyID;

        public Classes() { }

        public Classes(int _ClassID, string _Name, int _FacultyID)
        {
            SetClassID(_ClassID);
            SetName(_Name);
            SetFacultyID(_FacultyID);
        }

        public int GetClassID()
        {
            return ClassID;
        }

        public void SetClassID(int value)
        {
            ClassID = value;
        }

        public string GetName()
        {
            return Name;
        }

        public void SetName(string value)
        {
            Name = value;
        }

        public int GetFacultyID()
        {
            return FacultyID;
        }

        public void SetFacultyID(int value)
        {
            FacultyID = value;
        }

        /// <summary>
        /// Get Classes from Database by Class ID.
        /// </summary>
        /// <param name="_ClassID"></param>
        /// <returns></returns>
        public static Classes Get(int _ClassID)
        {
            string Query = $"select * from tb_Classes where ClassID = {_ClassID}";

            return Classes.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Get Classes from Database by Class Name.
        /// </summary>
        /// <param name="_ClassName"></param>
        /// <returns></returns>
        public static Classes Get(string _ClassName)
        {
            string Query = $"select * from tb_Classes where [Name] = '{_ClassName}'";

            return Classes.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Insert Classes to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_Classes([Name], FacultyID) values('{GetName()}', {GetFacultyID()})";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Get Faculty Name of Class by Class Name.
        /// </summary>
        /// <param name="_ClassName"></param>
        /// <returns></returns>
        public static string GetFacultyName(string _ClassName)
        {
            string Query = $"select dbo.func_GetFacultyNameByClassName('{_ClassName}')";

            return SQL_Sever.ExecuteScalar(Query).ToString();
        }

        /// <summary>
        /// Convert DataRow to Classes.
        /// </summary>
        /// <param name="_Row"></param>
        /// <returns></returns>
        public static Classes Parse(DataRow _Row)
        {
            return new Classes()
            {
                ClassID = Convert.ToInt32(_Row[0]),
                Name = Convert.ToString(_Row[1]),
                FacultyID = Convert.ToInt32(_Row[2])
            };
        }

        public static bool Exists(string _ClassName, string _FacultyName)
        {
            try
            {
                return (Classes.GetFacultyName(_ClassName) == _FacultyName);
            }
            catch { return false; }
        }
    }
}