﻿using System;
using System.Data;

namespace SICGang.Models
{
    public class Datalogs
    {
        private string StudentID;
        private DateTime Time;
        private string ModifiedContent;

        public Datalogs() { }

        public Datalogs(string _StudentID, DateTime _Time, string _ModifiedContent)
        {
            SetStudentID(_StudentID);
            SetTime(_Time);
            SetModifiedContent(_ModifiedContent);
        }

        public string GetStudentID()
        {
            return StudentID;
        }

        public void SetStudentID(string value)
        {
            StudentID = value;
        }

        public DateTime GetTime()
        {
            return Time;
        }

        public void SetTime(DateTime value)
        {
            Time = value;
        }

        public string GetModifiedContent()
        {
            return ModifiedContent;
        }

        public void SetModifiedContent(string value)
        {
            ModifiedContent = value;
        }

        /// <summary>
        /// Get Datalogs from Database by Username and Time.
        /// </summary>
        /// <param name="_User"></param>
        /// <param name="_Time"></param>
        /// <returns></returns>
        public static Datalogs Get(string _User, DateTime _Time)
        {
            string Query = $"select * from tb_Datalogs where [User] = '{_User}' and [Time] = '{_Time}'";

            return Datalogs.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Insert Datalogs to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_Datalogs values('{GetStudentID()}', '{GetTime()}', N'{GetModifiedContent()}')";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Convert DataRow to Datalogs.
        /// </summary>
        /// <param name="_Row"></param>
        /// <returns></returns>
        public static Datalogs Parse(DataRow _Row)
        {
            return new Datalogs()
            {
                StudentID = Convert.ToString(_Row[0]),
                Time = Convert.ToDateTime(_Row[1]),
                ModifiedContent = Convert.ToString(_Row[2])
            };
        }
    }
}