﻿using System;
using System.Data;
using System.Drawing;
using System.IO;

namespace SICGang.Models
{
    public class StudentIMGs
    {
        private string StudentID;
        private string IMGTextBase64;

        public StudentIMGs() { }

        public StudentIMGs(string _StudentID, string _ImgTextBase64)
        {
            SetStudentID(_StudentID);
            SetIMG(_ImgTextBase64);
        }

        public string GetStudentID()
        {
            return StudentID;
        }

        public void SetStudentID(string value)
        {
            StudentID = value;
        }

        public string GetBase64IMG()
        {
            return IMGTextBase64;
        }

        public Image GetIMG()
        {
            return Base64ToImage(IMGTextBase64);
        }

        public void SetIMG(string value)
        {
            IMGTextBase64 = value;
        }

        public void SetIMG(Image value)
        {
            IMGTextBase64 = ImageToBase64(value);
        }

        /// <summary>
        /// Get StudentImgs from Database by Student ID.
        /// </summary>
        /// <param name="_StudentID"></param>
        /// <returns></returns>
        public static StudentIMGs Get(string _StudentID)
        {
            string Query = $"select * from tb_StudentIMGs where StudentID = '{_StudentID}'";

            return StudentIMGs.Parse(SQL_Sever.ReadRow(Query));
        }

        /// <summary>
        /// Insert StudentImgs to Database and return result insert.
        /// </summary>
        /// <returns></returns>
        public bool Post()
        {
            string Query = $"insert tb_StudentIMGs values('{GetStudentID()}', '{GetBase64IMG()}')";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Update StudentIMGs by Student ID.
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string Query = $"update tb_StudentIMGs set IMGTextBase64 = '{GetBase64IMG()}' where StudentID = '{GetStudentID()}'";

            int RowsAffected = SQL_Sever.ExecuteCommand(Query);

            return (RowsAffected != 0);
        }

        /// <summary>
        /// Convert ImgTextBase64 to Image.
        /// </summary>
        /// <param name="_ImgTextBase64"></param>
        /// <returns></returns>
        public static Image Base64ToImage(string _ImgTextBase64)
        {
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(_ImgTextBase64)))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        /// <summary>
        /// Convert Image to Base64
        /// </summary>
        /// <param name="_Image"></param>
        /// <returns></returns>
        public static string ImageToBase64(Image _Image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                _Image.Save(ms, _Image.RawFormat);
                byte[] imageBytes = ms.ToArray();

                return Convert.ToBase64String(imageBytes);
            }
        }

        /// <summary>
        /// Convert DataRow to StudentImgs.
        /// </summary>
        /// <param name="_Row"></param>
        /// <returns></returns>
        public static StudentIMGs Parse(DataRow _Row)
        {
            return new StudentIMGs()
            {
                StudentID = Convert.ToString(_Row[0]),
                IMGTextBase64 = Convert.ToString(_Row[1])
            };
        }
    }
}