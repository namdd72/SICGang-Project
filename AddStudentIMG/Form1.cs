﻿using SICGang.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddStudentIMG
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Folder = textBox1.Text;
            //var ListStudentID = Directory.GetFiles(Folder);

            //var Student = new Students();
            //var StudentIMG = new StudentIMGs();
            //int count = 0;

            //foreach (var i in ListStudentID)
            //{
            //    try
            //    {
            //        count += 1;
            //        string MSV = i.Substring(i.LastIndexOf("\\") + 1, 10);
            //        //if (MSV.StartsWith("155") || MSV.StartsWith("165") || MSV.StartsWith("175") || MSV.StartsWith("185"))
            //        if (MSV.Length == 10)
            //        {
            //            try
            //            {
            //                Student.SetStudentID(MSV);
            //                Student.Post();
            //            }
            //            catch { }

            //            StudentIMG.SetStudentID(MSV);
            //            if (File.Exists(Folder + $"\\{MSV}.jpg"))
            //                StudentIMG.SetIMG(Image.FromFile(Folder + $"\\{MSV}.jpg"));
            //            else
            //                StudentIMG.SetIMG(Image.FromFile(Folder + $"\\{MSV}.JPG"));
            //            StudentIMG.Post();
            //        }
            //    }
            //    catch { }
            //}

            //label1.Text = $"{count}/{ListStudentID.Count()} ~ {count * 100 / ListStudentID.Count()}%";

            var ListStudentID = SQL_Sever.ExecuteList("select StudentID from tb_Students");

            var StudentIMG = new StudentIMGs();
            int count = 0;

            foreach (string StudentID in ListStudentID)
            {
                try
                {
                    StudentIMG.SetStudentID(StudentID);
                    if (File.Exists(Folder + $"\\{StudentID}.jpg"))
                    {
                        StudentIMG.SetIMG(Image.FromFile(Folder + $"\\{StudentID}.jpg"));
                        StudentIMG.Post();
                        count += 1;
                    }
                }
                catch { }
            }

            label1.Text = $"{count}/{ListStudentID.Count()} ~ {count * 100 / ListStudentID.Count()}%";

            MessageBox.Show("Done!");
        }
    }
}
